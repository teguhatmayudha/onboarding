package partnerChannel

import (
	partnerDomain "bitbucket.org/finaccelteam/onboarding/domain/partner"
	domain "bitbucket.org/finaccelteam/onboarding/domain/partnerChannel"
	"bitbucket.org/finaccelteam/onboarding/request"
	"bitbucket.org/finaccelteam/onboarding/response"
)

type service struct {
	repo        domain.Repository
	repoPartner partnerDomain.Repository
}

func mapDomainToResponse(partnerChannel *domain.PartnerChannel) response.PartnerChannel {
	return response.PartnerChannel{
		ID:          partnerChannel.ID,
		PartnerID:   partnerChannel.PartnerID,
		Code:        partnerChannel.Code,
		Name:        partnerChannel.Name,
		DisplayName: partnerChannel.DisplayName,
		IsActive:    partnerChannel.IsActive,
	}
}

func mapRequestToDomain(reqPartnerChannel request.PartnerChannel) domain.PartnerChannel {
	return domain.PartnerChannel{
		PartnerID:   reqPartnerChannel.PartnerID,
		Code:        reqPartnerChannel.Code,
		Name:        reqPartnerChannel.Name,
		DisplayName: reqPartnerChannel.DisplayName,
		IsActive:    reqPartnerChannel.IsActive,
		GrpCode:     reqPartnerChannel.GrpCode,
		CreatedBy:   "1",
		UpdatedBy:   "2",
	}
}

func NewService(repo domain.Repository, repoPartner partnerDomain.Repository) Service {
	return &service{
		repo:        repo,
		repoPartner: repoPartner,
	}
}

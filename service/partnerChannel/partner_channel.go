package partnerChannel

import (
	"bitbucket.org/finaccelteam/onboarding/domain/partnerChannel"
	"bitbucket.org/finaccelteam/onboarding/oerror"
	"bitbucket.org/finaccelteam/onboarding/request"
	"bitbucket.org/finaccelteam/onboarding/response"
	"context"
)

type Service interface {
	GetPartnerChannel(ctx context.Context, id int) (*response.PartnerChannel, error)
	FindByCode(ctx context.Context, Code string) (*response.PartnerChannel, error)
	CreatePartnerChannel(ctx context.Context, partnerChannel request.PartnerChannel) (int, error)
	UpdatePartnerChannel(ctx context.Context, reqPartnerChannel request.PartnerChannel, id int) error
}

func (s *service) GetPartnerChannel(ctx context.Context, id int) (*response.PartnerChannel, error) {
	pc, err := s.repo.FindByID(ctx, id)
	if err != nil {
		return nil, oerror.NewInternalServerErrorWithCause(oerror.InternalError, err, "find by code")
	}
	if pc == nil {
		return nil, oerror.NewBusinessValidationError(oerror.PartnerChannelNotFound, "id {%d} not found", id)
	}
	resp := mapDomainToResponse(pc)
	return &resp, nil
}

func (s *service) FindByCode(ctx context.Context, Code string) (*response.PartnerChannel, error) {
	resp, err := s.repo.FindByCode(ctx, Code)
	if err != nil {
		return nil, oerror.NewInternalServerErrorWithCause(oerror.InternalError, err, "find by code")
	}

	if resp == nil {
		return nil, oerror.NewBusinessValidationError(oerror.PartnerChannelNotFound, "code {%s} not found", Code)
	}
	respMapDomainToResponse := mapDomainToResponse(resp)
	return &respMapDomainToResponse, nil
}

func (s *service) CreatePartnerChannel(ctx context.Context, reqPartnerChannel request.PartnerChannel) (int, error) {
	partnerID, err := s.repoPartner.FindByID(ctx, reqPartnerChannel.PartnerID)
	if err != nil {
		return -1, oerror.NewInternalServerErrorWithCause(oerror.InternalError, err, "find by id")
	}

	if partnerID == nil {
		return -1, oerror.NewBusinessValidationError(oerror.PartnerNotFound, "partner with id {%d} not found", reqPartnerChannel.PartnerID)
	}

	reqPartnerChannelToDomain := mapRequestToDomain(reqPartnerChannel)
	id, err := s.repo.Save(ctx, &reqPartnerChannelToDomain)
	if err != nil {
		return id, oerror.NewInternalServerErrorWithCause(oerror.InternalError, err, "save")
	}
	return id, nil
}

func (s *service) UpdatePartnerChannel(ctx context.Context, reqPartnerChannel request.PartnerChannel, id int) error {
	partnerChannelResp, err := s.repo.FindByID(ctx, id)
	if err != nil {
		return oerror.NewInternalServerErrorWithCause(oerror.InternalError, err, "find by id")
	}

	if partnerChannelResp == nil {
		return oerror.NewBusinessValidationError(oerror.PartnerChannelNotFound, "id {%d} not found", id)
	}

	partnerResp, err := s.repoPartner.FindByID(ctx, reqPartnerChannel.PartnerID)
	if err != nil {
		return oerror.NewInternalServerErrorWithCause(oerror.InternalError, err, "find by id")
	}

	if partnerResp == nil {
		return oerror.NewBusinessValidationError(oerror.PartnerNotFound, "partner id {%d} not found", reqPartnerChannel.PartnerID)
	}

	reqToDomain := partnerChannel.PartnerChannel{
		ID:                     partnerChannelResp.ID,
		PartnerID:              reqPartnerChannel.PartnerID,
		Name:                   reqPartnerChannel.Name,
		DisplayName:            reqPartnerChannel.DisplayName,
		Code:                   reqPartnerChannel.Code,
		SecretKey:              partnerChannelResp.SecretKey,
		GrpCode:                reqPartnerChannel.GrpCode,
		Flag:                   partnerChannelResp.Flag,
		Type:                   partnerChannelResp.Type,
		LogoUrl:                partnerChannelResp.LogoUrl,
		MinimumAmount:          partnerChannelResp.MinimumAmount,
		IsManualPaymentAllowed: partnerChannelResp.IsManualPaymentAllowed,
		Ordering:               partnerChannelResp.Ordering,
		IsActive:               reqPartnerChannel.IsActive,
		NotifyUser:             partnerChannelResp.NotifyUser,
		DescriptionCode:        partnerChannelResp.DescriptionCode,
		Timezone:               partnerChannelResp.Timezone,
		Version:                partnerChannelResp.Version,
		CreatedBy:              partnerChannelResp.CreatedBy,
		UpdatedBy:              partnerChannelResp.UpdatedBy,
		DeletedBy:              partnerChannelResp.DeletedBy,
	}

	if err = s.repo.Update(ctx, &reqToDomain); err != nil {
		return oerror.NewInternalServerErrorWithCause(oerror.InternalError, err, "update")
	}

	return nil
}

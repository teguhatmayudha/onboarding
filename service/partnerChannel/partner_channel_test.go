package partnerChannel

import (
	"context"
	"database/sql"
	"testing"

	ferror "bitbucket.org/finaccelteam/ms-common-go/ferror/v2"
	domainPartner "bitbucket.org/finaccelteam/onboarding/domain/partner"
	partnerMock "bitbucket.org/finaccelteam/onboarding/domain/partner/mocks"
	domain "bitbucket.org/finaccelteam/onboarding/domain/partnerChannel"
	partnerChannelMock "bitbucket.org/finaccelteam/onboarding/domain/partnerchannel/mocks"
	"bitbucket.org/finaccelteam/onboarding/oerror"
	"bitbucket.org/finaccelteam/onboarding/request"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

const (
	GetPartnerChannel    = "GetPartnerChannel"
	FindByCode           = "FindByCode"
	CreatePartnerChannel = "CreatePartnerChannel"
	UpdatePartnerChannel = "UpdatePartnerChannel"
)

type PartnerChannelTestSuite struct {
	suite.Suite
	sqldb                 *sql.DB
	sqlmock               sqlmock.Sqlmock
	partnerChannelRepo    partnerChannelMock.Repository
	partnerRepo           partnerMock.Repository
	partnerChannelService Service
}

type partnerChannelTestCase struct {
	name              string
	request           *request.PartnerChannel
	prepareMock       func(*request.PartnerChannel, *testing.T)
	assertCalled      func(*testing.T, *request.PartnerChannel)
	expectedErrorCode ferror.Code
	wantErr           bool
	serviceName       string
}

// Before each test
func (suite *PartnerChannelTestSuite) Before() {
	suite.sqldb, suite.sqlmock, _ = sqlmock.New()
	suite.partnerRepo = partnerMock.Repository{}
	suite.partnerChannelRepo = partnerChannelMock.Repository{}
	suite.partnerChannelService = NewService(
		&suite.partnerChannelRepo,
		&suite.partnerRepo,
	)
}

func TestSuiteRun(t *testing.T) {
	suite.Run(t, new(PartnerChannelTestSuite))
}

func (suite *PartnerChannelTestSuite) TestPartnerChannel_GetPartnerChannel() {
	testCases := []partnerChannelTestCase{
		{
			name:        "PartnerChannel.FindByID() Success",
			request:     nil,
			wantErr:     false,
			serviceName: GetPartnerChannel,
			prepareMock: func(request *request.PartnerChannel, t *testing.T) {
				suite.partnerChannelRepo.On("FindByID", mock.Anything, 10).Return(&domain.PartnerChannel{}, nil).Once()
			},
			assertCalled: func(t *testing.T, request *request.PartnerChannel) {
				suite.partnerChannelRepo.AssertCalled(t, "FindByID", mock.Anything, 10)
			},
		},
		{
			name:              "PartnerChannel.FindByID() throw error repo",
			request:           nil,
			wantErr:           true,
			expectedErrorCode: oerror.InternalError,
			serviceName:       GetPartnerChannel,
			prepareMock: func(request *request.PartnerChannel, t *testing.T) {
				suite.partnerChannelRepo.On("FindByID", mock.Anything, 10).Return(nil, ferror.E("some error"))
			},
			assertCalled: func(t *testing.T, request *request.PartnerChannel) {
				suite.partnerChannelRepo.AssertCalled(t, "FindByID", mock.Anything, 10)
			},
		},
		{
			name:              "PartnerChannel.FindByID() not found",
			request:           nil,
			wantErr:           true,
			expectedErrorCode: oerror.PartnerChannelNotFound,
			serviceName:       GetPartnerChannel,
			prepareMock: func(request *request.PartnerChannel, t *testing.T) {
				suite.partnerChannelRepo.On("FindByID", mock.Anything, 10).Return(nil, nil)
			},
			assertCalled: func(t *testing.T, request *request.PartnerChannel) {
				suite.partnerChannelRepo.AssertCalled(t, "FindByID", mock.Anything, 10)
			},
		},
	}

	DoTestCases(testCases, suite)
}

func (suite *PartnerChannelTestSuite) TestPartnerChannel_FindByCode() {
	testCases := []partnerChannelTestCase{
		{
			name:        "PartnerChannel.FindByCode() success",
			request:     nil,
			wantErr:     false,
			serviceName: FindByCode,
			prepareMock: func(request *request.PartnerChannel, t *testing.T) {
				suite.partnerChannelRepo.On("FindByCode", mock.Anything, "code").Return(&domain.PartnerChannel{}, nil).Once()
			},
			assertCalled: func(t *testing.T, request *request.PartnerChannel) {
				suite.partnerChannelRepo.AssertCalled(t, "FindByCode", mock.Anything, "code")
			},
		},
		{
			name:              "PartnerChannel.FindByCode() throw error",
			request:           nil,
			wantErr:           true,
			expectedErrorCode: oerror.InternalError,
			serviceName:       FindByCode,
			prepareMock: func(request *request.PartnerChannel, t *testing.T) {
				suite.partnerChannelRepo.On("FindByCode", mock.Anything, "code").Return(nil, ferror.E("some error"))
			},
			assertCalled: func(t *testing.T, request *request.PartnerChannel) {
				suite.partnerChannelRepo.AssertCalled(t, "FindByCode", mock.Anything, "code")
			},
		},
		{
			name:              "PartnerChannel.FindByCode() not found",
			request:           nil,
			wantErr:           true,
			expectedErrorCode: oerror.PartnerChannelNotFound,
			serviceName:       FindByCode,
			prepareMock: func(request *request.PartnerChannel, t *testing.T) {
				suite.partnerChannelRepo.On("FindByCode", mock.Anything, "code").Return(nil, nil)
			},
			assertCalled: func(t *testing.T, request *request.PartnerChannel) {
				suite.partnerChannelRepo.AssertCalled(t, "FindByCode", mock.Anything, "code")
			},
		},
	}

	DoTestCases(testCases, suite)
}

func (suite *PartnerChannelTestSuite) TestPartnerChannel_CreatePartnerChannel() {
	testCases := []partnerChannelTestCase{
		{
			name:              "PartnerChannel.Save() partnerRepo.FindByID() throw error repo",
			request:           &request.PartnerChannel{PartnerID: 10, Code: "code", Name: "partner channel", DisplayName: "pc", IsActive: true, GrpCode: "grpcode"},
			wantErr:           true,
			expectedErrorCode: oerror.InternalError,
			serviceName:       CreatePartnerChannel,
			prepareMock: func(request *request.PartnerChannel, t *testing.T) {
				suite.partnerRepo.On("FindByID", mock.Anything, request.PartnerID).Return(nil, ferror.E("some error"))
			},
			assertCalled: func(t *testing.T, request *request.PartnerChannel) {
				suite.partnerRepo.AssertCalled(t, "FindByID", mock.Anything, request.PartnerID)
			},
		},
		{
			name:              "PartnerChannel.Save() partnerRepo.FindByID() not found",
			request:           &request.PartnerChannel{PartnerID: 10, Code: "code", Name: "partner channel", DisplayName: "pc", IsActive: true, GrpCode: "grpcode"},
			wantErr:           true,
			expectedErrorCode: oerror.PartnerNotFound,
			serviceName:       CreatePartnerChannel,
			prepareMock: func(request *request.PartnerChannel, t *testing.T) {
				suite.partnerRepo.On("FindByID", mock.Anything, request.PartnerID).Return(nil, nil).Once()
			},
			assertCalled: func(t *testing.T, request *request.PartnerChannel) {
				suite.partnerRepo.AssertCalled(t, "FindByID", mock.Anything, request.PartnerID)
			},
		},
		{
			name:        "PartnerChannel.Save() Success",
			request:     &request.PartnerChannel{PartnerID: 10, Code: "code", Name: "partner channel", DisplayName: "pc", IsActive: true, GrpCode: "grpcode"},
			wantErr:     false,
			serviceName: CreatePartnerChannel,
			prepareMock: func(request *request.PartnerChannel, t *testing.T) {
				reqToDomain := mapRequestToDomain(*request)
				suite.partnerRepo.On("FindByID", mock.Anything, request.PartnerID).Return(&domainPartner.Partner{}, nil).Once()
				suite.partnerChannelRepo.On("Save", mock.Anything, &reqToDomain).Return(1, nil).Once()
			},
			assertCalled: func(t *testing.T, request *request.PartnerChannel) {
				reqToDomain := mapRequestToDomain(*request)
				suite.partnerRepo.AssertCalled(t, "FindByID", mock.Anything, request.PartnerID)
				suite.partnerChannelRepo.AssertCalled(t, "Save", mock.Anything, &reqToDomain)
			},
		},
		{
			name:              "PartnerChannel.Save() throw error repo",
			request:           &request.PartnerChannel{PartnerID: 10, Code: "code", Name: "partner channel", DisplayName: "pc", IsActive: true, GrpCode: "grpcode"},
			wantErr:           true,
			expectedErrorCode: oerror.InternalError,
			serviceName:       CreatePartnerChannel,
			prepareMock: func(request *request.PartnerChannel, t *testing.T) {
				reqToDomain := mapRequestToDomain(*request)
				suite.partnerRepo.On("FindByID", mock.Anything, request.PartnerID).Return(&domainPartner.Partner{}, nil).Once()
				suite.partnerChannelRepo.On("Save", mock.Anything, &reqToDomain).Return(-1, ferror.E("some error")).Once()
			},
			assertCalled: func(t *testing.T, request *request.PartnerChannel) {
				reqToDomain := mapRequestToDomain(*request)
				suite.partnerRepo.AssertCalled(t, "FindByID", mock.Anything, request.PartnerID)
				suite.partnerChannelRepo.AssertCalled(t, "Save", mock.Anything, &reqToDomain)
			},
		},
	}

	DoTestCases(testCases, suite)
}

func (suite *PartnerChannelTestSuite) TestPartnerChannel_UpdatePartnerChannel() {
	testCases := []partnerChannelTestCase{
		{
			name:              "PartnerChannel.Update() FindByID() not found",
			request:           &request.PartnerChannel{PartnerID: 20, Code: "code", Name: "partner channel", DisplayName: "pc", IsActive: true, GrpCode: "grpcode"},
			wantErr:           true,
			expectedErrorCode: oerror.PartnerChannelNotFound,
			serviceName:       UpdatePartnerChannel,
			prepareMock: func(request *request.PartnerChannel, t *testing.T) {
				suite.partnerChannelRepo.On("FindByID", mock.Anything, 10).Return(nil, nil).Once()
			},
			assertCalled: func(t *testing.T, request *request.PartnerChannel) {
				suite.partnerChannelRepo.AssertCalled(t, "FindByID", mock.Anything, 10)
			},
		},
		{
			name:              "PartnerChannel.Update() partnerRepo.FindByID() throw error repo",
			request:           &request.PartnerChannel{PartnerID: 20, Code: "code", Name: "partner channel", DisplayName: "pc", IsActive: true, GrpCode: "grpcode"},
			wantErr:           true,
			expectedErrorCode: oerror.InternalError,
			serviceName:       UpdatePartnerChannel,
			prepareMock: func(request *request.PartnerChannel, t *testing.T) {
				suite.partnerChannelRepo.On("FindByID", mock.Anything, 10).Return(nil, ferror.E("some error")).Once()
			},
			assertCalled: func(t *testing.T, request *request.PartnerChannel) {
				suite.partnerChannelRepo.AssertCalled(t, "FindByID", mock.Anything, 10)
			},
		},
		{
			name:              "PartnerChannel.Update() FindByID() not found",
			request:           &request.PartnerChannel{PartnerID: 20, Code: "code", Name: "partner channel", DisplayName: "pc", IsActive: true, GrpCode: "grpcode"},
			wantErr:           true,
			expectedErrorCode: oerror.PartnerNotFound,
			serviceName:       UpdatePartnerChannel,
			prepareMock: func(request *request.PartnerChannel, t *testing.T) {
				suite.partnerChannelRepo.On("FindByID", mock.Anything, 10).Return(&domain.PartnerChannel{}, nil).Once()
				suite.partnerRepo.On("FindByID", mock.Anything, request.PartnerID).Return(nil, nil).Once()
			},
			assertCalled: func(t *testing.T, request *request.PartnerChannel) {
				suite.partnerChannelRepo.AssertCalled(t, "FindByID", mock.Anything, 10)
				suite.partnerRepo.AssertCalled(t, "FindByID", mock.Anything, request.PartnerID)
			},
		},
		{
			name:              "PartnerChannel.Update() FindByID() throw error repo",
			request:           &request.PartnerChannel{PartnerID: 20, Code: "code", Name: "partner channel", DisplayName: "pc", IsActive: true, GrpCode: "grpcode"},
			wantErr:           true,
			expectedErrorCode: oerror.InternalError,
			serviceName:       UpdatePartnerChannel,
			prepareMock: func(request *request.PartnerChannel, t *testing.T) {
				suite.partnerChannelRepo.On("FindByID", mock.Anything, 10).Return(&domain.PartnerChannel{}, nil).Once()
				suite.partnerRepo.On("FindByID", mock.Anything, request.PartnerID).Return(nil, ferror.E("some error")).Once()
			},
			assertCalled: func(t *testing.T, request *request.PartnerChannel) {
				suite.partnerChannelRepo.AssertCalled(t, "FindByID", mock.Anything, 10)
				suite.partnerRepo.AssertCalled(t, "FindByID", mock.Anything, request.PartnerID)
			},
		},
		{
			name:              "PartnerChannel.Update() throw error repo",
			request:           &request.PartnerChannel{PartnerID: 20, Code: "code", Name: "partner channel", DisplayName: "pc", IsActive: true, GrpCode: "grpcode"},
			wantErr:           true,
			expectedErrorCode: oerror.InternalError,
			serviceName:       UpdatePartnerChannel,
			prepareMock: func(request *request.PartnerChannel, t *testing.T) {
				req := RequestToDomain(request)
				suite.partnerChannelRepo.On("FindByID", mock.Anything, 10).Return(&domain.PartnerChannel{}, nil).Once()
				suite.partnerRepo.On("FindByID", mock.Anything, request.PartnerID).Return(&domainPartner.Partner{}, nil).Once()
				suite.partnerChannelRepo.On("Update", mock.Anything, req).Return(ferror.E("some error")).Once()
			},
			assertCalled: func(t *testing.T, request *request.PartnerChannel) {
				req := RequestToDomain(request)
				suite.partnerChannelRepo.AssertCalled(t, "FindByID", mock.Anything, 10)
				suite.partnerRepo.AssertCalled(t, "FindByID", mock.Anything, request.PartnerID)
				suite.partnerChannelRepo.AssertCalled(t, "Update", mock.Anything, req)
			},
		},
		{
			name:        "PartnerChannel.Update() Success",
			request:     &request.PartnerChannel{PartnerID: 20, Code: "code", Name: "partner channel", DisplayName: "pc", IsActive: true, GrpCode: "grpcode"},
			wantErr:     false,
			serviceName: UpdatePartnerChannel,
			prepareMock: func(request *request.PartnerChannel, t *testing.T) {
				req := RequestToDomain(request)
				suite.partnerChannelRepo.On("FindByID", mock.Anything, 10).Return(&domain.PartnerChannel{}, nil).Once()
				suite.partnerRepo.On("FindByID", mock.Anything, request.PartnerID).Return(&domainPartner.Partner{}, nil).Once()
				suite.partnerChannelRepo.On("Update", mock.Anything, req).Return(nil).Once()
			},
			assertCalled: func(t *testing.T, request *request.PartnerChannel) {
				req := RequestToDomain(request)
				suite.partnerChannelRepo.AssertCalled(t, "FindByID", mock.Anything, 10)
				suite.partnerRepo.AssertCalled(t, "FindByID", mock.Anything, request.PartnerID)
				suite.partnerChannelRepo.AssertCalled(t, "Update", mock.Anything, req)
			},
		},
	}

	DoTestCases(testCases, suite)
}

func DoTestCases(testCases []partnerChannelTestCase, suite *PartnerChannelTestSuite) {
	for _, testCase := range testCases {
		suite.T().Run(testCase.name, func(t *testing.T) {
			suite.Before()

			testCase.prepareMock(testCase.request, t)

			err := DoService(testCase, suite)

			if testCase.wantErr {
				assert.True(t, ferror.Is(err, testCase.expectedErrorCode), "error expected: %v, but got: %v", testCase.expectedErrorCode, err)
			}
			assert.Equal(t, testCase.wantErr, err != nil, "error expected %v, but got: %v", testCase.wantErr, err)

			testCase.assertCalled(t, testCase.request)
		})
	}
}

func DoService(testCase partnerChannelTestCase, suite *PartnerChannelTestSuite) error {
	var (
		ctx = context.Background()
		err error
	)

	switch testCase.serviceName {
	case GetPartnerChannel:
		_, err = suite.partnerChannelService.GetPartnerChannel(ctx, 10)
	case FindByCode:
		_, err = suite.partnerChannelService.FindByCode(ctx, "code")
	case CreatePartnerChannel:
		_, err = suite.partnerChannelService.CreatePartnerChannel(ctx, *testCase.request)
	case UpdatePartnerChannel:
		err = suite.partnerChannelService.UpdatePartnerChannel(ctx, *testCase.request, 10)
	}
	return err
}

func RequestToDomain(req *request.PartnerChannel) *domain.PartnerChannel {
	return &domain.PartnerChannel{
		PartnerID:   req.PartnerID,
		Code:        req.Code,
		Name:        req.Name,
		DisplayName: req.DisplayName,
		IsActive:    req.IsActive,
		GrpCode:     req.GrpCode,
	}
}

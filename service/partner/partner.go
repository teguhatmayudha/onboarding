package partner

import (
	domain "bitbucket.org/finaccelteam/onboarding/domain/partner"
	"bitbucket.org/finaccelteam/onboarding/oerror"
	"bitbucket.org/finaccelteam/onboarding/request"
	"bitbucket.org/finaccelteam/onboarding/response"
	"context"
)

type Service interface {
	GetPartners(ctx context.Context) ([]response.Partner, error)
	GetPartner(ctx context.Context, id int) (*response.Partner, error)
	CreatePartner(ctx context.Context, partner request.Partner) (int, error)
	UpdatePartner(ctx context.Context, partner request.Partner, id int) error
	FindByMerchantID(ctx context.Context, merchantId string) ([]response.Partner, error)
	FindByGatewayType(ctx context.Context, gatewayType string) ([]response.Partner, error)
	FindByMerchantIDAndGatewayType(ctx context.Context, merchantId, gatewayType string) ([]response.Partner, error)
}

func (s *service) FindByMerchantIDAndGatewayType(ctx context.Context, merchantID, gatewayType string) ([]response.Partner, error) {
	partners, err := s.repo.FindByMerchantIDAndGatewayType(ctx, merchantID, gatewayType)
	if err != nil {
		return nil, oerror.NewInternalServerErrorWithCause(oerror.InternalError, err, "find by id and gateway type")
	}

	partnersResp := responsePartners(partners)
	return partnersResp, nil
}

func (s *service) FindByGatewayType(ctx context.Context, gatewayType string) ([]response.Partner, error) {
	partners, err := s.repo.FindByGatewayType(ctx, gatewayType)
	if err != nil {
		return nil, oerror.NewInternalServerErrorWithCause(oerror.InternalError, err, "find by gateway type")
	}

	partnersResp := responsePartners(partners)
	return partnersResp, nil
}

func (s *service) GetPartners(ctx context.Context) ([]response.Partner, error) {
	partners, err := s.repo.Get(ctx)
	if err != nil {
		return nil, oerror.NewInternalServerErrorWithCause(oerror.InternalError, err, "get")
	}

	partnersResp := responsePartners(partners)
	return partnersResp, nil
}

func (s *service) GetPartner(ctx context.Context, id int) (*response.Partner, error) {
	partner, err := s.repo.FindByID(ctx, id)
	if err != nil {
		return nil, oerror.NewInternalServerErrorWithCause(oerror.InternalError, err, "find by id")
	}
	if partner == nil {
		return nil, oerror.NewBusinessValidationError(oerror.PartnerNotFound, "id {%d} not found", id)
	}
	partnersResp := mapDomainToResponse(partner)
	return &partnersResp, nil
}

func (s *service) CreatePartner(ctx context.Context, reqPartner request.Partner) (int, error) {
	reqToDomain := mapRequestToDomain(reqPartner)

	id, err := s.repo.Save(ctx, &reqToDomain)
	if err != nil {
		return -1, oerror.NewInternalServerErrorWithCause(oerror.InternalError, err, "save")
	}

	return id, nil
}

func (s *service) UpdatePartner(ctx context.Context, reqPartner request.Partner, id int) error {
	partner, err := s.repo.FindByID(ctx, id)
	if err != nil {
		return oerror.NewInternalServerErrorWithCause(oerror.InternalError, err, "find by id")
	}
	if partner == nil {
		return oerror.NewBusinessValidationError(oerror.PartnerNotFound, "id {%d} not found", id)
	}

	reqToDomain := domain.Partner{
		ID:          partner.ID,
		Name:        reqPartner.Name,
		MerchantID:  reqPartner.MerchantID,
		GatewayType: reqPartner.GatewayType,
		IsActive:    *reqPartner.IsActive,
		Version:     partner.Version,
		UpdatedBy:   partner.UpdatedBy,
	}

	if err = s.repo.Update(ctx, &reqToDomain); err != nil {
		return oerror.NewInternalServerErrorWithCause(oerror.InternalError, err, "update")
	}
	return nil
}

func (s *service) FindByMerchantID(ctx context.Context, merchantID string) ([]response.Partner, error) {
	partners, err := s.repo.FindByMerchantID(ctx, merchantID)
	if err != nil {
		return nil, oerror.NewInternalServerErrorWithCause(oerror.InternalError, err, "find by merchant id")
	}

	partnersResp := responsePartners(partners)
	return partnersResp, nil
}

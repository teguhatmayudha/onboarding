package partner

import (
	"context"
	"database/sql"
	"testing"

	ferror "bitbucket.org/finaccelteam/ms-common-go/ferror/v2"
	"bitbucket.org/finaccelteam/onboarding/domain/partner"
	partnerMock "bitbucket.org/finaccelteam/onboarding/domain/partner/mocks"
	"bitbucket.org/finaccelteam/onboarding/oerror"
	"bitbucket.org/finaccelteam/onboarding/request"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/suite"
	"gopkg.in/DATA-DOG/go-sqlmock.v1"
)

const (
	UpdatePartner                  = "UpdatePartner"
	FindByMerchantIDAndGatewayType = "FindByMerchantIDAndGatewayType"
	FindByGatewayType              = "FindByGatewayType"
	GetPartner                     = "GetPartner"
	FindByMerchantID               = "FindByMerchantID"
	GetPartners                    = "GetPartners"
	Save                           = "Save"
)

type PartnerTestSuite struct {
	suite.Suite
	sqldb          *sql.DB
	sqlmock        sqlmock.Sqlmock
	partnerRepo    partnerMock.Repository
	partnerService Service
}

type partnerTestCase struct {
	name              string
	request           *request.Partner
	prepareMock       func(*request.Partner, *testing.T)
	assertCalled      func(*testing.T, *request.Partner)
	expectedErrorCode ferror.Code
	wantErr           bool
	serviceName       string
}

// Before each test
func (suite *PartnerTestSuite) Before() {
	suite.sqldb, suite.sqlmock, _ = sqlmock.New()
	suite.partnerRepo = partnerMock.Repository{}
	suite.partnerService = NewService(
		&suite.partnerRepo,
	)
}

func TestSuiteRun(t *testing.T) {
	suite.Run(t, new(PartnerTestSuite))
}

func (suite *PartnerTestSuite) TestPartner_Save() {
	True := true

	testCases := []partnerTestCase{
		{
			name:        "Partner.Save() Success",
			wantErr:     false,
			serviceName: Save,
			request:     &request.Partner{Name: "testing test", MerchantID: "merchant", GatewayType: 2, IsActive: &True, EntityID: 4},
			prepareMock: func(request *request.Partner, t *testing.T) {
				reqToDomain := mapRequestToDomain(*request)
				suite.partnerRepo.On("Save", mock.Anything, &reqToDomain).Return(1, nil).Once()
			},
			assertCalled: func(t *testing.T, request *request.Partner) {
				reqToDomain := mapRequestToDomain(*request)
				suite.partnerRepo.AssertCalled(t, "Save", mock.Anything, &reqToDomain)
			},
		},
		{
			name:              "Partner.Save() throw error repo",
			expectedErrorCode: oerror.InternalError,
			serviceName:       Save,
			request:           &request.Partner{Name: "testing test", MerchantID: "merchant_id", GatewayType: 2, IsActive: &True, EntityID: 4},
			wantErr:           true,
			prepareMock: func(request *request.Partner, t *testing.T) {
				reqToDomain := mapRequestToDomain(*request)
				suite.partnerRepo.On("Save", mock.Anything, &reqToDomain).Return(-1, ferror.E("Error"))
			},
			assertCalled: func(t *testing.T, request *request.Partner) {
				reqToDomain := mapRequestToDomain(*request)
				suite.partnerRepo.AssertCalled(t, "Save", mock.Anything, &reqToDomain)
			},
		},
	}

	DoTestCases(testCases, suite)
}

func (suite *PartnerTestSuite) TestPartner_GetAll() {
	testCases := []partnerTestCase{
		{
			name:        "Partner.GetPartners() Success",
			wantErr:     false,
			serviceName: GetPartners,
			request:     nil,
			prepareMock: func(request *request.Partner, t *testing.T) {
				suite.partnerRepo.On("Get", mock.Anything).Return(nil, nil).Once()
			},
			assertCalled: func(t *testing.T, request *request.Partner) {
				suite.partnerRepo.AssertCalled(t, "Get", mock.Anything)
			},
		},
		{
			name:              "Partner.GetPartners() throw error repo",
			expectedErrorCode: oerror.InternalError,
			serviceName:       GetPartners,
			wantErr:           true,
			request:           nil,
			prepareMock: func(request *request.Partner, t *testing.T) {
				suite.partnerRepo.On("Get", mock.Anything).Return(nil, ferror.E("some error"))
			},
			assertCalled: func(t *testing.T, request *request.Partner) {
				suite.partnerRepo.AssertCalled(t, "Get", mock.Anything)
			},
		},
	}

	DoTestCases(testCases, suite)
}

func (suite *PartnerTestSuite) TestPartner_FindByMerchantID() {
	testCases := []partnerTestCase{
		{
			name:        "Partner.FindByMerchantID() success",
			wantErr:     false,
			serviceName: FindByMerchantID,
			request:     nil,
			prepareMock: func(request *request.Partner, t *testing.T) {
				suite.partnerRepo.On("FindByMerchantID", mock.Anything, "merchant_id").Return(nil, nil).Once()
			},
			assertCalled: func(t *testing.T, request *request.Partner) {
				suite.partnerRepo.AssertCalled(t, "FindByMerchantID", mock.Anything, "merchant_id")
			},
		},
		{
			name:              "Partner.FindByMerchantID() throw error",
			serviceName:       FindByMerchantID,
			expectedErrorCode: oerror.InternalError,
			wantErr:           true,
			request:           nil,
			prepareMock: func(request *request.Partner, t *testing.T) {
				suite.partnerRepo.On("FindByMerchantID", mock.Anything, "merchant_id").Return(nil, ferror.E("some error"))
			},
			assertCalled: func(t *testing.T, request *request.Partner) {
				suite.partnerRepo.AssertCalled(t, "FindByMerchantID", mock.Anything, "merchant_id")
			},
		},
	}
	DoTestCases(testCases, suite)
}

func (suite *PartnerTestSuite) TestPartner_GetPartner() {

	testCases := []partnerTestCase{
		{
			name:        "Partner.GetPartner() success",
			serviceName: GetPartner,
			wantErr:     false,
			request:     nil,
			prepareMock: func(request *request.Partner, t *testing.T) {
				suite.partnerRepo.On("FindByID", mock.Anything, 20).Return(&partner.Partner{}, nil).Once()
			},
			assertCalled: func(t *testing.T, request *request.Partner) {
				suite.partnerRepo.AssertCalled(t, "FindByID", mock.Anything, 20)
			},
		},
		{
			name:              "Partner.GetPartner() not found",
			expectedErrorCode: oerror.PartnerNotFound,
			serviceName:       GetPartner,
			wantErr:           true,
			request:           nil,
			prepareMock: func(request *request.Partner, t *testing.T) {
				suite.partnerRepo.On("FindByID", mock.Anything, 20).Return(nil, nil)
			},
			assertCalled: func(t *testing.T, request *request.Partner) {
				suite.partnerRepo.AssertCalled(t, "FindByID", mock.Anything, 20)
			},
		},
		{
			name:              "Partner.GetPartner() throw error repo",
			wantErr:           true,
			serviceName:       GetPartner,
			expectedErrorCode: oerror.InternalError,
			request:           nil,
			prepareMock: func(request *request.Partner, t *testing.T) {
				suite.partnerRepo.On("FindByID", mock.Anything, 20).Return(nil, ferror.E("some error"))
			},
			assertCalled: func(t *testing.T, request *request.Partner) {
				suite.partnerRepo.AssertCalled(t, "FindByID", mock.Anything, 20)
			},
		},
	}

	DoTestCases(testCases, suite)

}

func (suite *PartnerTestSuite) TestPartner_FindByGatewayType() {
	testCases := []partnerTestCase{
		{
			name:        "Partner.FindByGatewayType() success",
			serviceName: FindByGatewayType,
			wantErr:     false,
			request:     nil,
			prepareMock: func(request *request.Partner, t *testing.T) {
				suite.partnerRepo.On("FindByGatewayType", mock.Anything, "10").Return([]*partner.Partner{}, nil).Once()
			},
			assertCalled: func(t *testing.T, request *request.Partner) {
				suite.partnerRepo.AssertCalled(t, "FindByGatewayType", mock.Anything, "10")
			},
		},
		{
			name:              "Partner.FindByGatewayType() throw error repo",
			wantErr:           true,
			serviceName:       FindByGatewayType,
			request:           nil,
			expectedErrorCode: oerror.InternalError,
			prepareMock: func(request *request.Partner, t *testing.T) {
				suite.partnerRepo.On("FindByGatewayType", mock.Anything, "10").Return(nil, ferror.E("some error"))
			},
			assertCalled: func(t *testing.T, request *request.Partner) {
				suite.partnerRepo.AssertCalled(t, "FindByGatewayType", mock.Anything, "10")
			},
		},
	}

	DoTestCases(testCases, suite)
}

func (suite *PartnerTestSuite) TestPartner_FindByMerchantIDAndGatewayType() {
	testCases := []partnerTestCase{
		{
			name:        "Partner.FindByMerchantIDAndGatewayType() success",
			serviceName: FindByMerchantIDAndGatewayType,
			request:     nil,
			wantErr:     false,
			prepareMock: func(request *request.Partner, t *testing.T) {
				suite.partnerRepo.On("FindByMerchantIDAndGatewayType", mock.Anything, "merchant_id", "2").Return(nil, nil).Once()
			},
			assertCalled: func(t *testing.T, request *request.Partner) {
				suite.partnerRepo.AssertCalled(t, "FindByMerchantIDAndGatewayType", mock.Anything, "merchant_id", "2")
			},
		},
		{
			name:              "Partner.FindByMerchantIDAndGatewayType() throw error repo",
			serviceName:       FindByMerchantIDAndGatewayType,
			wantErr:           true,
			request:           nil,
			expectedErrorCode: oerror.InternalError,
			prepareMock: func(request *request.Partner, t *testing.T) {
				suite.partnerRepo.On("FindByMerchantIDAndGatewayType", mock.Anything, "merchant_id", "2").Return(nil, ferror.E("some error"))
			},
			assertCalled: func(t *testing.T, request *request.Partner) {
				suite.partnerRepo.AssertCalled(t, "FindByMerchantIDAndGatewayType", mock.Anything, "merchant_id", "2")
			},
		},
	}

	DoTestCases(testCases, suite)
}

func (suite *PartnerTestSuite) TestPartner_UpdatePartner() {
	True := true

	testCases := []partnerTestCase{
		{
			name:        "Partner.UpdatePartner() success",
			wantErr:     false,
			serviceName: UpdatePartner,
			request:     &request.Partner{Name: "testing test", MerchantID: "merchant_id_003", GatewayType: 2, IsActive: &True},
			prepareMock: func(request *request.Partner, t *testing.T) {
				reqToDomain := RequestToDomain(request)
				suite.partnerRepo.On("FindByID", mock.Anything, 10).Return(&partner.Partner{}, nil).Once()
				suite.partnerRepo.On("Update", mock.Anything, reqToDomain).Return(nil).Once()
			},
			assertCalled: func(t *testing.T, request *request.Partner) {
				reqToDomain := RequestToDomain(request)
				suite.partnerRepo.AssertCalled(t, "FindByID", mock.Anything, 10)
				suite.partnerRepo.AssertCalled(t, "Update", mock.Anything, reqToDomain)
			},
		},
		{
			name:              "Partner.UpdatePartner() FindByID() throw error repo",
			wantErr:           true,
			serviceName:       UpdatePartner,
			expectedErrorCode: oerror.InternalError,
			request:           &request.Partner{Name: "testing test", MerchantID: "merchant_id_003", GatewayType: 2, IsActive: &True},
			prepareMock: func(request *request.Partner, t *testing.T) {
				suite.partnerRepo.On("FindByID", mock.Anything, 10).Return(nil, ferror.E("some error"))
			},
			assertCalled: func(t *testing.T, request *request.Partner) {
				suite.partnerRepo.AssertCalled(t, "FindByID", mock.Anything, 10)

			},
		},
		{
			name:              "Partner.UpdatePartner() not found",
			wantErr:           true,
			serviceName:       UpdatePartner,
			expectedErrorCode: oerror.PartnerNotFound,
			request:           &request.Partner{Name: "testing test", MerchantID: "merchant_id_003", GatewayType: 2, IsActive: &True},
			prepareMock: func(request *request.Partner, t *testing.T) {
				suite.partnerRepo.On("FindByID", mock.Anything, 10).Return(nil, nil).Once()
			},
			assertCalled: func(t *testing.T, request *request.Partner) {
				suite.partnerRepo.AssertCalled(t, "FindByID", mock.Anything, 10)

			},
		},
		{
			name:              "Partner.UpdatePartner() throw error",
			wantErr:           true,
			serviceName:       UpdatePartner,
			expectedErrorCode: oerror.InternalError,
			request:           &request.Partner{Name: "testing test", MerchantID: "merchant_id_003", GatewayType: 2, IsActive: &True},
			prepareMock: func(request *request.Partner, t *testing.T) {
				reqToDomain := RequestToDomain(request)
				suite.partnerRepo.On("FindByID", mock.Anything, 10).Return(&partner.Partner{}, nil).Once()
				suite.partnerRepo.On("Update", mock.Anything, reqToDomain).Return(ferror.E("some error")).Once()
			},
			assertCalled: func(t *testing.T, request *request.Partner) {
				reqToDomain := RequestToDomain(request)
				suite.partnerRepo.AssertCalled(t, "FindByID", mock.Anything, 10)
				suite.partnerRepo.AssertCalled(t, "Update", mock.Anything, reqToDomain)
			},
		},
	}

	DoTestCases(testCases, suite)

}

func DoTestCases(testCases []partnerTestCase, suite *PartnerTestSuite) {
	for _, testCase := range testCases {
		suite.T().Run(testCase.name, func(t *testing.T) {
			suite.Before()

			testCase.prepareMock(testCase.request, t)

			err := DoService(testCase, suite)

			if testCase.wantErr {
				assert.True(t, ferror.Is(err, testCase.expectedErrorCode), "error expected: %v, but got: %v", testCase.expectedErrorCode, err)
			}
			assert.Equal(t, testCase.wantErr, err != nil, "error expected %v, but got: %v", testCase.wantErr, err)

			testCase.assertCalled(t, testCase.request)
		})
	}
}

func DoService(testCase partnerTestCase, suite *PartnerTestSuite) error {
	var (
		ctx = context.Background()
		err error
	)

	switch testCase.serviceName {
	case UpdatePartner:
		err = suite.partnerService.UpdatePartner(ctx, *testCase.request, 10)
	case FindByMerchantIDAndGatewayType:
		_, err = suite.partnerService.FindByMerchantIDAndGatewayType(ctx, "merchant_id", "2")
	case FindByGatewayType:
		_, err = suite.partnerService.FindByGatewayType(ctx, "10")
	case GetPartner:
		_, err = suite.partnerService.GetPartner(ctx, 20)
	case FindByMerchantID:
		_, err = suite.partnerService.FindByMerchantID(ctx, "merchant_id")
	case GetPartners:
		_, err = suite.partnerService.GetPartners(ctx)
	case Save:
		_, err = suite.partnerService.CreatePartner(ctx, *testCase.request)
	}

	return err
}

func RequestToDomain(req *request.Partner) *partner.Partner {
	return &partner.Partner{
		MerchantID:  req.MerchantID,
		Name:        req.Name,
		IsActive:    *req.IsActive,
		GatewayType: req.GatewayType,
	}
}

package partner

import (
	"bitbucket.org/finaccelteam/ms-common-go/sql"
	domain "bitbucket.org/finaccelteam/onboarding/domain/partner"
	"bitbucket.org/finaccelteam/onboarding/request"
	"bitbucket.org/finaccelteam/onboarding/response"
)

func mapDomainToResponse(partner *domain.Partner) response.Partner {
	return response.Partner{
		ID:          partner.ID,
		MerchantID:  partner.MerchantID,
		Name:        partner.Name,
		IsActive:    partner.IsActive,
		GatewayType: partner.GatewayType,
		EntityID:    partner.EntityID,
	}
}

func mapRequestToDomain(req request.Partner) domain.Partner {
	return domain.Partner{
		MerchantID:  req.MerchantID,
		Name:        req.Name,
		IsActive:    *req.IsActive,
		GatewayType: req.GatewayType,
		EntityID:    req.EntityID,
		Timezone:    "Asia/Jakarta",
		CreatedBy:   "1",
		UpdatedBy:   sql.GetNullString("1"),
	}
}

func responsePartners(partners []*domain.Partner) []response.Partner {
	partnersResp := make([]response.Partner, 0)
	for _, partner := range partners {
		tPartner := mapDomainToResponse(partner)
		partnersResp = append(partnersResp, tPartner)
	}
	return partnersResp
}

package partner

import (
	"bitbucket.org/finaccelteam/onboarding/domain/partner"
)

type service struct {
	repo partner.Repository
}

func NewService(repo partner.Repository) Service {
	return &service{
		repo: repo,
	}
}

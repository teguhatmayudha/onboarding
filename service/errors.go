package service

import "bitbucket.org/finaccelteam/ms-common-go/ferror"

const (
	// ErrRepository is the super-type for any error that is coming from the repository layer.
	//
	// This error type IS eligible for retry.
	ErrRepository = ferror.Code("ONBOARDING_SERVICE-1001")

	// ErrBusinessValidation is anything that fails our business requirement.
	//
	// This error type IS eligible for retry.
	ErrBusinessValidation = ferror.Code("ONBOARDING_SERVICE-1002")

	// ErrUnprocessable is for any condition that will fail even on multiple retries, so we need Developer intervention
	// to solve these types of errors.
	//
	// This error type is NOT eligible for retry.
	ErrUnprocessable = ferror.Code("ONBOARDING_SERVICE-1003")

	// ErrInternal is any other error that can occur during execution.
	//
	// This error type IS eligible for retry.
	ErrInternal = ferror.Code("ONBOARDING_SERVICE-1004")

	ErrNotFound = ferror.Code("ONBOARDING_SERVICE-1005")
)

package kafka

import (
	kafka "bitbucket.org/finaccelteam/ms-common-go/broker/kafka"
)

var kafkaConfig Config

//KafkaConfig - Configuration struct for kafka subscriber
type Config struct {
	BootstrapServer        string     `mapstructure:"bootstrapServers"`
	SASLConfig             SASLConfig `mapstructure:"sasl_config"`
	Awk                    string     `mapstructure:"ack"`
	DisableSSLVerification *bool      `mapstructure:"disableSSLVerification"`
}

type SASLConfig struct {
	Username   string `mapstructure:"username"`
	Password   string `mapstructure:"password"`
	Protocol   string `mapstructure:"protocol"`
	Mechanisms string `mapstructure:"mechanisms"`
}

//LoadInto - returns the pointer to the struct
func (k *Config) LoadInto() interface{} {
	return &k
}

//GetKafkaConfig - return the pointer to the var GetKafkaConfig
func GetKafkaConfig() *Config {
	return &kafkaConfig
}

//GetSASLKafkaConfig - return SASLKafkaConfig
func GetSASLKafkaConfig(config SASLConfig) kafka.SASLConfig {
	return kafka.SASLConfig{
		Username:   config.Username,
		Protocol:   config.Protocol,
		Mechanisms: config.Mechanisms,
		Password:   config.Password,
	}
}

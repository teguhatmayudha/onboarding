package subscriber

import (
	"bitbucket.org/finaccelteam/ms-common-go/broker/kafka"
)

type ConsumerConfig struct {
	TopicName               string `mapstructure:"topicName"`
	Group                   string `mapstructure:"group"`
	NumberOfConsumerWorkers int    `mapstructure:"numberOfConsumerWorkers"`
	Dlq                     string `mapstructure:"dlq"`
	IsDlq                   bool
	TopicRetryConfigs       []TopicRetryConfig `mapstructure:"topic_retry_configs"`
}

type TopicRetryConfig struct {
	NumberOfRetry           int `mapstructure:"numberOfRetry"`
	BackOffTimeInSec        int `mapstructure:"backOffTimeInSec"`
	NumberOfConsumerWorkers int `mapstructure:"numberOfConsumerWorkers"`
}

// LoadInto - returns the pointer to the struct
func (c *ConsumerConfig) LoadInto() interface{} {
	return &c
}

func GetKafkaConsumerConfig(config *ConsumerConfig) kafka.ConsumerConfig {
	retryConfigs := make([]kafka.TopicRetryConfig, 0)
	for _, retry := range config.TopicRetryConfigs {
		if retry.NumberOfConsumerWorkers > 0 && retry.BackOffTimeInSec > 0 && retry.NumberOfRetry > 0 {
			retryConfig := kafka.TopicRetryConfig{
				NumberOfConsumerWorkers: retry.NumberOfConsumerWorkers,
				BackOffTimeInSec:        retry.BackOffTimeInSec,
				NumberOfRetry:           retry.NumberOfRetry,
			}
			retryConfigs = append(retryConfigs, retryConfig)
		}
	}
	consumerConfig := kafka.ConsumerConfig{
		Group:                   config.Group,
		TopicName:               config.TopicName,
		Dlq:                     config.Dlq,
		NumberOfConsumerWorkers: config.NumberOfConsumerWorkers,
		TopicRetryConfigs:       retryConfigs,
	}
	return consumerConfig
}

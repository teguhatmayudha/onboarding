package subscriber

import (
	"bitbucket.org/finaccelteam/ms-common-go/broker"
	"bitbucket.org/finaccelteam/ms-common-go/broker/handlers"
	"bitbucket.org/finaccelteam/ms-common-go/broker/kafka"
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	kafkaBrokerConfig "bitbucket.org/finaccelteam/onboarding/broker/kafka"
)

func GetKafkaEventSubscriber(consumerHandlerConfig *ConsumerConfig, isDlq bool, numberOfConsumers int) (broker.Subscriber, error) {
	var disableSSLVerificationOption kafka.SubscriberOption
	var offsetOption kafka.SubscriberOption
	var subscriberOptions []kafka.SubscriberOption

	//consumer config
	kafkaConfig := kafkaBrokerConfig.GetKafkaConfig()
	consumerConfig := GetKafkaConsumerConfig(consumerHandlerConfig)

	//dlq subs option
	dlqOption := kafka.SubscriberIsDlqOption(isDlq)
	subscriberOptions = append(subscriberOptions, dlqOption)

	////sasl subs option
	//saslConfig := kafkaBrokerConfig.GetSASLKafkaConfig(kafkaConfig.SASLConfig)
	//sslOptions := kafka.SubscriberSASLConfigOption(saslConfig)
	//subscriberOptions = append(subscriberOptions, sslOptions)

	//ssl subs option
	if kafkaConfig.DisableSSLVerification != nil {
		disableSSLVerificationOption = kafka.SubscriberDisableSSLVerificationOption(kafkaConfig.DisableSSLVerification)
		subscriberOptions = append(subscriberOptions, disableSSLVerificationOption)
	}

	if isDlq {
		consumerConfig.NumberOfConsumerWorkers = numberOfConsumers
		offsetOption = kafka.SubscriberAutoOffsetResetOption(kafka.PartitionOffsetEarliest)
	}
	subscriberOptions = append(subscriberOptions, offsetOption)

	// Registering Subcriber
	//sub, err := kafka.NewSubscriber(kafkaConfig.BootstrapServer, consumerConfig, disableSSLVerificationOption, sslOptions)
	sub, err := kafka.NewSubscriber(kafkaConfig.BootstrapServer, consumerConfig, subscriberOptions...)
	if err != nil {
		return nil, ferror.Wrap(err, "new kafka subscriber")
	}

	sub.Use(handlers.PanicRecovery)

	return sub, nil
}

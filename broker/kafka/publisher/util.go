package publisher

import (
	"bitbucket.org/finaccelteam/onboarding/enum"
	"bitbucket.org/finaccelteam/onboarding/oerror"
	"context"
	"strconv"
	"time"

	"bitbucket.org/finaccelteam/ms-common-go/broker"
	"bitbucket.org/finaccelteam/ms-common-go/broker/kafka"
	"bitbucket.org/finaccelteam/ms-common-go/logger"
	"bitbucket.org/finaccelteam/ms-common-go/types"
	outgoingEventDomain "bitbucket.org/finaccelteam/onboarding/domain/outgoingEvent"
)

var outgoingEventTypeToTopicMap map[int]string

func SetKafkaPublishedEventToTopicMap(ctx context.Context, consumer string) {
	outgoingEventTypeToTopicMap = make(map[int]string)
	switch consumer {
	case enum.Processes.Partner:
		//event := GetKafkaPublisherConfig()
		outgoingEventTypeToTopicMap[enum.OutgoingEvent.PartnerCreatedEvent] = "partnerOG"
	case enum.Processes.PartnerChannel:
		//event := GetKafkaPublisherConfig()
		outgoingEventTypeToTopicMap[enum.OutgoingEvent.PartnerChannelCreatedEvent] = "partnerChannelOG"
	default:
		logger.Fatal(ctx, "Invalid consumer flag. Please provide a valid consumer flag.")
	}
}

func PublishOutgoingEvents(ctx context.Context, eventPublisher broker.Publisher, outgoingEventRepo outgoingEventDomain.Repository, requestID string) error {
	var keyOptions broker.PublishOption
	var publisherOptions []broker.PublishOption
	var topic string

	// check data first if already publish before
	events, err := outgoingEventRepo.FindByIncomingRequestIDAndIsDispatched(ctx, requestID, false)
	if err != nil {
		return oerror.NewInternalServerErrorWithCause(oerror.InternalError, err, "find by incoming request id And is dispatched")
	}

	// if data is not found in database, return nil
	if len(events) == 0 {
		logger.Info(ctx, "No undispatched event found for incoming request id %s.", requestID)
		return nil
	}

	// set identity of where messages come from, and set on header
	headerMaps := map[string][]byte{
		"Event-Publisher-Name": []byte("Onboarding-MS"),
	}
	headers := kafka.PublishKafkaHeadersOption(headerMaps)
	publisherOptions = append(publisherOptions, headers)
	logger.Debug(ctx, "Publisher header: %v", headerMaps)

	//publish events in a loop
	for _, event := range events {
		topic = outgoingEventTypeToTopicMap[event.EventType]

		// if data have partition key
		if event.PartitionKey != nil {
			partitionKey := []byte(strconv.Itoa(*event.PartitionKey))
			keyOptions = kafka.PublishPartitioningKeyOption(partitionKey)
			publisherOptions = append(publisherOptions, keyOptions)
		}

		// publish data to specific topic, eventID, message and options
		if err = eventPublisher.Publish(ctx, topic, event.EventID, []byte(event.Payload), publisherOptions...); err != nil {
			return oerror.NewInternalServerErrorWithCause(oerror.InternalError, err, "publish")
		}

		//set dispatch status
		event.IsDispatched = true
		event.DispatchedAt = types.GetNullInt64(time.Now().Unix())

		// update the event data on db
		err = outgoingEventRepo.Update(ctx, event)
		if err != nil {
			return oerror.NewInternalServerErrorWithCause(oerror.InternalError, err, "update")
		}
	}

	return nil
}

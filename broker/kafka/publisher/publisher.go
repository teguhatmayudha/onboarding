package publisher

import (
	"bitbucket.org/finaccelteam/ms-common-go/broker"
	"bitbucket.org/finaccelteam/ms-common-go/broker/kafka"
	kafkaConfig "bitbucket.org/finaccelteam/onboarding/broker/kafka"
)

var (
	kafkaPublisher broker.Publisher
)

func GetKafkaEventPublisher() (broker.Publisher, error) {
	if kafkaPublisher == nil {
		config := kafkaConfig.GetKafkaConfig()
		var disableSSLVerificationOption kafka.PublisherOption
		if config.DisableSSLVerification != nil {
			disableSSLVerificationOption = kafka.PublisherDisableSSLVerificationOption(config.DisableSSLVerification)
		}
		var awk kafka.PublisherOption
		awk = kafka.PublisherAwkOption(config.Awk)
		return kafka.NewPublisher(config.BootstrapServer, disableSSLVerificationOption, awk)
	}
	return kafkaPublisher, nil
}

package publisher

var kafkaPublisherConfig KafkaPublisherConfig

type KafkaPublisherConfig struct {
	TopicName string `mapstructure:"topicName"`
}

func (c *KafkaPublisherConfig) LoadInto() interface{} {
	return &c
}

func GetKafkaPublisherConfig() *KafkaPublisherConfig {
	return &kafkaPublisherConfig
}

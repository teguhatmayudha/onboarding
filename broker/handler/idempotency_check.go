package handler

import (
	"bitbucket.org/finaccelteam/ms-common-go/broker"
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/ms-common-go/logger"
	"bitbucket.org/finaccelteam/ms-common-go/sql"
	domain "bitbucket.org/finaccelteam/onboarding/domain/incomingEvent"
	"bitbucket.org/finaccelteam/onboarding/enum"
	"context"
)

type IdempotencyMiddleWare struct {
	tx        sql.Transactional
	eventType int
	repo      domain.IncomingEventRepository
}

func (i *IdempotencyMiddleWare) Middleware() func(h broker.Handler) broker.Handler {
	return func(h broker.Handler) broker.Handler {
		return broker.HandlerFunc(func(ctx context.Context, m broker.PublishedMessage) error {
			incomingEventID := m.EventID()
			val := m.Header()["topic"]
			if val == "partners" {
				i.eventType = enum.IncomingEventType.Partner
			}
			if val == "partnerchannel" {
				i.eventType = enum.IncomingEventType.PartnerChannel
			}

			err := i.tx.WithTransaction(ctx, func(ctx context.Context) error {
				//idempotency check
				isProcessed, err := i.isEventProcessed(ctx,
					incomingEventID,
					i.eventType)
				if err != nil {
					logger.Info(ctx, "Failed to check event idempotency for event ID %s. Error: %v",
						incomingEventID, err)
					return err
				}

				if isProcessed {
					logger.Info(ctx, "Event is already processed for event ID %s", incomingEventID)
					return nil
				}

				if err = h.Consume(ctx, m); err != nil {
					if err, ok := err.(*broker.BadMessageError); ok {
						return broker.NewBadMessageError(err.Code(), err, "[brokerapm.Middleware] error while consuming message")
					}

					err = ferror.Wrap(err, "[brokerapm.Middleware] error while consuming message")
					return err
				}

				//save incoming event
				event := &domain.IncomingEvent{
					EventID:   incomingEventID,
					EventType: i.eventType,
					Version:   0,
				}

				_, err = i.repo.Save(ctx, event)
				if err != nil {
					return err
				}

				return nil
			})
			if err != nil {
				return ferror.Wrap(err, "WIth txn")
			}
			return nil
		})
	}
}

func (i *IdempotencyMiddleWare) isEventProcessed(ctx context.Context,
	eventID string,
	eventType int,
) (bool, error) {
	incomingEvent, err := i.repo.FindByEventIDAndEventType(ctx, eventID, eventType)
	if err != nil {
		return false, err
	}

	if incomingEvent != nil {
		return true, nil
	}

	return false, nil
}

func NewIdempotencyMiddleWare(tx sql.Transactional, repo domain.IncomingEventRepository) (*IdempotencyMiddleWare, error) {
	return &IdempotencyMiddleWare{
		tx:   tx,
		repo: repo,
	}, nil
}

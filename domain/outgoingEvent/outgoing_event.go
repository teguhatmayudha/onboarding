package outgoingEvent

import (
	"bitbucket.org/finaccelteam/ms-common-go/types"
	"context"
	"time"
)

//OutgoingEventRepository interface to implement persistence for OutgoingEvent domain
type Repository interface {
	Save(context.Context, *OutgoingEvent) (int, error)
	Update(context.Context, *OutgoingEvent) error
	FindByID(context.Context, int) (*OutgoingEvent, error)
	FindByOutgoingEventType(context.Context, int) ([]*OutgoingEvent, error)
	FindUnpublishedOutgoingEvents(context.Context) ([]*OutgoingEvent, error)
	FindByEventTypeAndIncomingRequestIDAndIsDispatched(context.Context, int, string, bool) ([]*OutgoingEvent, error)
	FindByIncomingRequestIDAndIsDispatched(context.Context, string, bool) ([]*OutgoingEvent, error)
	HardDeleteByCreatedAt(context.Context, time.Time, int) (int, error)
}

//OutgoingEvent domain
type OutgoingEvent struct {
	ID                int
	PartitionKey      *int
	EventType         int
	EventID           string
	IncomingRequestID string
	Payload           string
	DispatchedAt      types.NullInt64
	IsDispatched      bool
	Version           int
	CreatedAt         time.Time
	UpdatedAt         *time.Time
	DeletedAt         *time.Time
}

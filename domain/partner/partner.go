package partner

import (
	"bitbucket.org/finaccelteam/ms-common-go/sql"
	"context"
	"time"
)

// Repository interface to implement persistence for Partner domain
type Repository interface {
	Save(context.Context, *Partner) (int, error)
	Update(context.Context, *Partner) error
	FindByID(context.Context, int) (*Partner, error)
	Get(ctx context.Context) ([]*Partner, error)
	FindByMerchantID(context.Context, string) ([]*Partner, error)
	FindByMerchantIDAndGatewayType(context.Context, string, string) ([]*Partner, error)
	FindByGatewayType(context.Context, string) ([]*Partner, error)
}

// Partner domain
type Partner struct {
	ID          int
	Name        string
	MerchantID  string
	GatewayType int
	IsActive    bool
	EntityID    int
	Timezone    string
	Version     int
	CreatedAt   time.Time
	CreatedBy   string
	UpdatedAt   *time.Time
	UpdatedBy   sql.NullString
	DeletedAt   *time.Time
	DeletedBy   sql.NullString
}

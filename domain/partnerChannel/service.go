package partnerChannel

import (
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/onboarding/domain/partner"
	svError "bitbucket.org/finaccelteam/onboarding/service"
	"context"
)

type Service interface {
	Create(ctx context.Context, PartnerID int, Code, Name, DisplayName, GrpCode string, IsActive bool) (int, error)
}

type service struct {
	repo        Repository
	partnerRepo partner.Repository
}

func (s *service) Create(ctx context.Context, PartnerID int, Code, Name, DisplayName, GrpCode string, IsActive bool) (int, error) {
	partnerID, err := s.partnerRepo.FindByID(ctx, PartnerID)
	if err != nil {
		return -1, ferror.Wrap(err, "get partner by id")
	}

	if partnerID == nil {
		return -1, ferror.E("partner not found")
	}

	reqPartnerChannelToDomain := PartnerChannel{
		PartnerID:   PartnerID,
		Code:        Code,
		Name:        Name,
		DisplayName: DisplayName,
		GrpCode:     GrpCode,
		IsActive:    IsActive,
		Version:     1,
	}
	id, err := s.repo.Save(ctx, &reqPartnerChannelToDomain)
	if err != nil {
		return -1, ferror.NewWithCause(svError.ErrRepository, err, "create partner channel")
	}
	return id, nil
}

func NewService(repo Repository, partnerRepo partner.Repository) Service {
	return &service{
		repo:        repo,
		partnerRepo: partnerRepo,
	}
}

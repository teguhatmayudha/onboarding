package incomingEvent

import (
	"context"
	"time"
)

// IncomingEventRepository interface to implement persistence for IncomingEvent domain
type IncomingEventRepository interface {
	Save(context.Context, *IncomingEvent) (int, error)
	Update(context.Context, *IncomingEvent) error
	FindByID(context.Context, int) (*IncomingEvent, error)
	FindByEventID(context.Context, string) (*IncomingEvent, error)
	FindByEventIDAndEventType(context.Context, string, int) (*IncomingEvent, error)
}

// IncomingEvent domain
type IncomingEvent struct {
	ID        int
	EventID   string
	EventType int
	Version   int
	CreatedAt time.Time
	UpdatedAt *time.Time
	DeletedAt *time.Time
}

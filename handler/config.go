package handler

import (
	"bitbucket.org/finaccelteam/onboarding/broker/kafka/publisher"
	"bitbucket.org/finaccelteam/onboarding/broker/kafka/subscriber"
)

var (
	partnerDefaultKafkaConfig        subscriber.ConsumerConfig
	partnerChannelDefaultKafkaConfig subscriber.ConsumerConfig
	partnerPublisherConfig           publisher.KafkaPublisherConfig
)

func GetPartnerDefaultKafkaConfig() *subscriber.ConsumerConfig {
	return &partnerDefaultKafkaConfig
}

func GetPartnerChannelDefaultKafkaConfig() *subscriber.ConsumerConfig {
	return &partnerChannelDefaultKafkaConfig
}

func GetPartnerPublisherConfig() *publisher.KafkaPublisherConfig {
	return &partnerPublisherConfig
}

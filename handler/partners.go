package handler

import (
	"context"
	"encoding/json"

	commonBroker "bitbucket.org/finaccelteam/ms-common-go/broker"
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/ms-common-go/logger"
	"bitbucket.org/finaccelteam/onboarding/broker/kafka/publisher"
	"bitbucket.org/finaccelteam/onboarding/domain/incomingEvent"
	outgoingEventDomain "bitbucket.org/finaccelteam/onboarding/domain/outgoingEvent"
	"bitbucket.org/finaccelteam/onboarding/enum"
	"bitbucket.org/finaccelteam/onboarding/oerror"
	"bitbucket.org/finaccelteam/onboarding/request"
	"bitbucket.org/finaccelteam/onboarding/service/partner"
	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
)

type partnerDefault struct {
	defaultSvc        partner.Service
	defaultSubscriber commonBroker.Subscriber
	defaultValidator  *validator.Validate
	defaultPublisher  commonBroker.Publisher
	incomingEventRepo incomingEvent.IncomingEventRepository
	outgoingEventRepo outgoingEventDomain.Repository
}

func (pd *partnerDefault) validate(defaultRequest *request.Partner) (*request.Partner, error) {
	if err := pd.defaultValidator.Struct(defaultRequest); err != nil {
		return nil, oerror.NewUnprocessableWithCause(oerror.BadMessage, err, "validate")
	}
	return defaultRequest, nil
}

func (pd *partnerDefault) StartSubscriber() error {
	if err := pd.defaultSubscriber.Subscribe(context.Background(), pd.Execute); err != nil {
		return err
	}
	return nil
}

func (pd *partnerDefault) Execute(ctx context.Context, msg commonBroker.PublishedMessage) error {
	incomingEventID := msg.EventID()
	ctx = context.WithValue(ctx, "Incoming-Event-ID", incomingEventID)

	//call handler
	if err := pd.partnerDefaultHandler(ctx, msg); err != nil {
		return ferror.Wrap(err, "Partner Default Handler")
	}
	eventID := uuid.New().String()
	payload := string(msg.Message())
	//save incoming event
	event := &outgoingEventDomain.OutgoingEvent{
		EventID:           eventID,
		IncomingRequestID: incomingEventID,
		Payload:           payload,
		EventType:         enum.IncomingEventType.Partner,
		Version:           0,
	}

	_, err := pd.outgoingEventRepo.Save(ctx, event)
	if err != nil {
		return ferror.Wrap(err, "Save")
	}

	// publish outgoing events
	err = publisher.PublishOutgoingEvents(ctx,
		pd.defaultPublisher,
		pd.outgoingEventRepo,
		incomingEventID)
	if err != nil {
		return ferror.Wrap(err, "Publish Outgoing Events")
	}
	logger.Info(ctx, "Successfully published Created event for incomingEventID %v", incomingEventID)
	return nil
}

func (pd *partnerDefault) partnerDefaultHandler(ctx context.Context, m commonBroker.PublishedMessage) error {
	var partnerDefaultRequest request.Partner

	if err := json.Unmarshal(m.Message(), &partnerDefaultRequest); err != nil {
		// return commonBroker.NewBadMessageError(oerror.BadMessage, err, "json marshal")
		return commonBroker.NewBadMessageError(ferror.Code("BAD_MESSAGE"), err, "json marshal")
	}

	partnerDefaultReq, err := pd.validate(&partnerDefaultRequest)
	if err != nil {
		// return commonBroker.NewBadMessageError(oerror.BadMessage, err, "validate")
		return commonBroker.NewBadMessageError(ferror.Code("BAD_MESSAGE"), err, "validate")
	}

	_, err = pd.defaultSvc.CreatePartner(ctx, *partnerDefaultReq)
	if err != nil {
		// return commonBroker.NewError(oerror.InternalError, err, "create partner")
		return commonBroker.NewBadMessageError(ferror.Code("BAD_MESSAGE"), err, "create partner")
	}

	return nil
}

func NewPartnerDefault(kafkaSubscriber commonBroker.Subscriber, service partner.Service, kafkaPublisher commonBroker.Publisher, incomingEventRepo incomingEvent.IncomingEventRepository,
	outgoingEventRepo outgoingEventDomain.Repository) (EventHandler, error) {
	dv, err := registerDefaultValidator()
	if err != nil {
		logger.Fatal(context.Background(), "failed to register validator : %v", err)
	}
	return &partnerDefault{
		defaultSubscriber: kafkaSubscriber,
		defaultSvc:        service,
		defaultValidator:  dv,
		defaultPublisher:  kafkaPublisher,
		outgoingEventRepo: outgoingEventRepo,
		incomingEventRepo: incomingEventRepo,
	}, nil
}

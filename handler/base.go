package handler

import (
	"context"
	"github.com/go-playground/validator/v10"
)

var (
	eventHandlers []EventHandler
)

type EventHandler interface {
	StartSubscriber() error
}

//RegisterEventHandler - Registers event handler
func RegisterEventHandler(handler EventHandler) {
	eventHandlers = append(eventHandlers, handler)
}

type EventHandlerCloser interface {
	Close(context.Context) error
	Closed() <-chan struct{}
}

func registerDefaultValidator() (*validator.Validate, error) {
	validate := validator.New()

	return validate, nil
}

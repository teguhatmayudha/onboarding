package handler

import (
	"context"
	"encoding/json"

	commonBroker "bitbucket.org/finaccelteam/ms-common-go/broker"
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/ms-common-go/logger"
	"bitbucket.org/finaccelteam/onboarding/broker/kafka/publisher"
	"bitbucket.org/finaccelteam/onboarding/domain/incomingEvent"
	outgoingEventDomain "bitbucket.org/finaccelteam/onboarding/domain/outgoingEvent"
	"bitbucket.org/finaccelteam/onboarding/enum"
	"bitbucket.org/finaccelteam/onboarding/oerror"
	"bitbucket.org/finaccelteam/onboarding/request"
	"bitbucket.org/finaccelteam/onboarding/service/partnerChannel"
	"github.com/go-playground/validator/v10"
	"github.com/google/uuid"
)

type partnerChannelDefault struct {
	defaultSvc        partnerChannel.Service
	defaultSubscriber commonBroker.Subscriber
	defaultValidator  *validator.Validate
	defaultPublisher  commonBroker.Publisher
	incomingEventRepo incomingEvent.IncomingEventRepository
	outgoingEventRepo outgoingEventDomain.Repository
}

func (pd *partnerChannelDefault) validate(defaultRequest *request.PartnerChannel) (*request.PartnerChannel, error) {
	if err := pd.defaultValidator.Struct(defaultRequest); err != nil {
		return nil, oerror.NewBusinessValidationErrorWithCause(oerror.BadMessage, err, "validate")
	}
	return defaultRequest, nil
}

func (pd *partnerChannelDefault) StartSubscriber() error {
	if err := pd.defaultSubscriber.Subscribe(context.Background(), pd.Execute); err != nil {
		return err
	}
	return nil
}

func (pd *partnerChannelDefault) Execute(ctx context.Context, msg commonBroker.PublishedMessage) error {
	incomingEventID := msg.EventID()
	ctx = context.WithValue(ctx, "Incoming-Event-ID", incomingEventID)

	//call handler
	if err := pd.partnerChannelDefaultHandler(ctx, msg); err != nil {
		return ferror.Wrap(err, "Partner Channel Default Handler")
	}
	eventID := uuid.New().String()
	payload := string(msg.Message())
	//save incoming event
	event := &outgoingEventDomain.OutgoingEvent{
		EventID:           eventID,
		IncomingRequestID: incomingEventID,
		Payload:           payload,
		EventType:         enum.IncomingEventType.PartnerChannel,
		Version:           0,
	}

	_, err := pd.outgoingEventRepo.Save(ctx, event)
	if err != nil {
		return ferror.Wrap(err, "Save")
	}

	// publish outgoing events
	err = publisher.PublishOutgoingEvents(ctx,
		pd.defaultPublisher,
		pd.outgoingEventRepo,
		incomingEventID)
	if err != nil {
		return ferror.Wrap(err, "Publish Outgoing Events")
	}
	logger.Info(ctx, "Successfully published Created event for incomingEventID %v", incomingEventID)
	return nil
}

func (pd *partnerChannelDefault) partnerChannelDefaultHandler(ctx context.Context, m commonBroker.PublishedMessage) error {
	var partnerChannelDefaultRequest request.PartnerChannel

	if err := json.Unmarshal(m.Message(), &partnerChannelDefaultRequest); err != nil {
		// return commonBroker.NewBadMessageError(oerror.BadMessage, err, "json marshal")
		return commonBroker.NewBadMessageError(ferror.Code("BAD_MESSAGE"), err, "json marshal")
	}

	if _, err := pd.validate(&partnerChannelDefaultRequest); err != nil {
		// return commonBroker.NewBadMessageError(oerror.BadMessage, err, "validate")
		return commonBroker.NewBadMessageError(ferror.Code("BAD_MESSAGE"), err, "validate")
	}

	if _, err := pd.defaultSvc.CreatePartnerChannel(ctx, partnerChannelDefaultRequest); err != nil {
		// return commonBroker.NewError(oerror.InternalError, err, "create partner")
		return commonBroker.NewBadMessageError(ferror.Code("BAD_MESSAGE"), err, "create partner")
	}

	return nil
}

func NewPartnerChannelDefault(kafkaSubscriber commonBroker.Subscriber, service partnerChannel.Service, kafkaPublisher commonBroker.Publisher, incomingEventRepo incomingEvent.IncomingEventRepository,
	outgoingEventRepo outgoingEventDomain.Repository) (EventHandler, error) {
	dv, err := registerDefaultValidator()
	if err != nil {
		logger.Fatal(context.Background(), "failed to register validator : %v", err)
	}
	return &partnerChannelDefault{
		defaultSubscriber: kafkaSubscriber,
		defaultSvc:        service,
		defaultValidator:  dv,
		defaultPublisher:  kafkaPublisher,
		outgoingEventRepo: outgoingEventRepo,
		incomingEventRepo: incomingEventRepo,
	}, nil
}

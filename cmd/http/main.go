package main

import (
	"bitbucket.org/finaccelteam/ms-common-go/config"
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/ms-common-go/fhttp/middleware"
	"bitbucket.org/finaccelteam/ms-common-go/logger"
	"bitbucket.org/finaccelteam/onboarding/broker/kafka"
	"bitbucket.org/finaccelteam/onboarding/container"
	"bitbucket.org/finaccelteam/onboarding/handler"
	"context"
	"github.com/gorilla/mux"
	"github.com/shopspring/decimal"
	"os"
)

//const (
//	channelDefault = "web"
//)

func main() {
	ctx := context.Background()
	router := mux.NewRouter()
	decimal.MarshalJSONWithoutQuotes = true

	setConfig(ctx)

	router.Use(middleware.ResponseTime)
	// pass optional params to skip url from authorisation
	//	router.Use(commonMiddleware.Authorization(authConfig.AuthKey))
	router.Use(middleware.CorrelationID)

	err := container.CreateHTTPContainer(ctx, router)
	if err != nil {
		logger.Fatal(ctx, "Error in initRoutes Error %v", ferror.StackTrace(err))
	}
	////create http server
	httpServer, err := container.InitServer(router)
	if err != nil {
		logger.Fatal(ctx, "Error while initialising server %s", err)
	}

	//Start http server
	logger.Info(ctx, "Starting http server...")
	err = httpServer.Start(ctx)
	if err != nil {
		logger.Error(ctx, "Error on starting up Http Server. %v", ferror.StackTrace(err))
	}
}

func setConfig(ctx context.Context) {
	//Get Config path if available in env var
	configPath := os.Getenv("CONFIG_PATH")
	if configPath == "" {
		configPath = "./config.yml"
	}

	registerConfigs()

	//load config
	if err := config.LoadConfig(configPath); err != nil {
		logger.Fatal(ctx, "Unable to load config. Error: %v.", err.Error())
	}

	//Set set auth token and create slac client
	/*	slackClient, err := commonSlack.New(slack.GetConfig().Channels["authtoken"])
		if err != nil {
			logger.Fatal(ctx, "could not initialise slack")
		}

		var loggerOption logger.Option
		if channel, ok := slack.GetConfig().Channels["web"]; ok {
			loggerOption = logger.SlackClientOption(slackClient, channel)
		}*/

	//set service name in logger
	if err := logger.Initialise(logger.INFO, logger.JSONFormat, "ms-payment[Web]", "stg"); err != nil {
		logger.Fatal(ctx, "could not initialise logger")
	}
}

func registerConfigs() {
	config.Register("server", container.GetServerConfig())
	config.Register("database", container.GetMySQLDBConfig())
	config.Register("kafka", kafka.GetKafkaConfig())
	config.Register("publisher_kafka_config_for_partner", handler.GetPartnerPublisherConfig())
}

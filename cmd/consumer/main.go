package main

import (
	"bitbucket.org/finaccelteam/ms-common-go/config"
	"bitbucket.org/finaccelteam/ms-common-go/logger"
	"bitbucket.org/finaccelteam/onboarding/broker/kafka"
	"bitbucket.org/finaccelteam/onboarding/broker/kafka/publisher"
	"bitbucket.org/finaccelteam/onboarding/container"
	"bitbucket.org/finaccelteam/onboarding/enum"
	"bitbucket.org/finaccelteam/onboarding/handler"
	"context"
	"flag"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"time"
)

func main() {
	var consumer string
	var isDLQueue bool
	var numberOfConsumer int
	var eventHandler handler.EventHandler
	var err error

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// this flag for DLQ
	flag.StringVar(&consumer, "consumer", enum.Processes.PartnerChannel, "Type of consumer to run")
	flag.BoolVar(&isDLQueue, "dlq", false, "Run this consumer on Dead Letter Queue")
	flag.IntVar(&numberOfConsumer, "numberOfConsumer", 1, "Number of consumer to run on Dead Letter Queue")
	flag.Parse()

	switch consumer {
	case enum.Processes.Partner:
		setAllConfig(ctx, consumer)

		eventHandler, err = container.GetPartnerDefaultEventHandler(ctx, isDLQueue, numberOfConsumer)
		if err != nil {
			logger.Fatal(ctx, "Error creating partner default event handler. %v", err.Error())
		}
	case enum.Processes.PartnerChannel:
		setAllConfig(ctx, consumer)

		eventHandler, err = container.GetPartnerChannelDefaultEventHandler(ctx, isDLQueue, numberOfConsumer)
		if err != nil {
			logger.Fatal(ctx, "Error creating partner channel default event handler. %v", err.Error())
		}
	default:
		logger.Fatal(ctx, "Invalid consumer flag. Please provide a valid consumer flag.")
	}

	defer func() {
		if err := container.CloseConnections(); err != nil {
			logger.Fatal(ctx, "failed to close connections of container : ", err)
		}
	}()

	var wg sync.WaitGroup
	exit := make(chan os.Signal, 1)
	signal.Notify(exit, os.Interrupt, syscall.SIGTERM)

	// subscribe to queue
	if err := eventHandler.StartSubscriber(); err != nil {
		logger.Fatal(ctx, "Error subscribing to %v queue. Error: %v", consumer, err.Error())
	}

	switch v := eventHandler.(type) {
	case handler.EventHandlerCloser:
		{
			closed := v.Closed()

			select {
			case <-exit:
				logger.Info(ctx, "Shutting down %v consumer....", consumer)
				err := v.Close(context.Background())
				if err != nil {
					logger.Error(ctx, "Error while closing subscriber")
					break
				}
				select {
				case <-closed:
					logger.Info(ctx, "Subscriber closed successfully")
				case <-time.After(30 * time.Second):
					logger.Info(ctx, "Subscriber closed forcefully")
				}
			case <-closed:
				{
					logger.Info(ctx, "Subscriber closed successfully")
				}
			}
		}
	default:
		{
			<-exit
			logger.Info(ctx, "Subscriber closed successfully")
			close(exit)
			// Wait for worker to shut down
			wg.Wait()
			// Block and exit on interrupt
			logger.Info(ctx, "Subscriber closed successfully")
		}
	}

	logger.Info(ctx, "Exiting %v consumer", consumer)
	os.Exit(0)
}

func loadConfig(ctx context.Context) {
	//Get Config path if available in env var
	configPath := os.Getenv("CONFIG_PATH")
	if configPath == "" {
		configPath = "./config.yml"
	}

	if err := config.LoadConfig(configPath); err != nil {
		logger.Fatal(ctx, "failed to load configs : ", err)
	}
}

//func setConfig(ctx context.Context, consumer string) {
//	loadConfig(ctx)
//
//	switch consumer {
//	case enum.Processes.PartnerConsumer:
//		registerPartnerConsumerConfig()
//	case enum.Processes.PartnerChannelConsumer:
//		registerPartnerChannelConsumerConfig()
//	default:
//		logger.Fatal(ctx, "Invalid consumer flag. Please provide a valid consumer flag.")
//	}
//}

func setAllConfig(ctx context.Context, consumer string) {
	//register all configs
	registerConfigs()

	loadConfig(ctx)

	publisher.SetKafkaPublishedEventToTopicMap(ctx, consumer)
}

//func registerPartnerConsumerConfig() {
//	config.Register("database", container.GetMySQLDBConfig())
//	config.Register("kafka", kafka.GetKafkaConfig())
//	config.Register("subscriber_kafka_config_for_partner_channel", handler.GetPartnerChannelDefaultKafkaConfig())
//}
//
//func registerPartnerChannelConsumerConfig() {
//	config.Register("database", container.GetMySQLDBConfig())
//	config.Register("kafka", kafka.GetKafkaConfig())
//	config.Register("subscriber_kafka_config_for_partner", handler.GetPartnerDefaultKafkaConfig())
//}

func registerConfigs() {
	config.Register("database", container.GetMySQLDBConfig())
	config.Register("kafka", kafka.GetKafkaConfig())
	config.Register("subscriber_kafka_config_for_partner", handler.GetPartnerDefaultKafkaConfig())
	config.Register("subscriber_kafka_config_for_partner_channel", handler.GetPartnerChannelDefaultKafkaConfig())
	config.Register("publisher_kafka_config_for_partner", handler.GetPartnerPublisherConfig())
}

package request

type Partner struct {
	Name        string `validate:"required,min=1" json:"name"`
	MerchantID  string `validate:"required,min=2" json:"merchant_id"`
	GatewayType int    `validate:"required,min=1" json:"gateway_type"`
	IsActive    *bool  `validate:"required" json:"is_active"`
	EntityID    int    `validate:"required,min=1" json:"entity_id"`
}

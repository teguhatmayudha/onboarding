package request

type PartnerChannel struct {
	PartnerID   int    `validate:"required,min=1" json:"partner_id"`
	Code        string `validate:"required,min=2" json:"code"`
	Name        string `validate:"required,min=2" json:"name"`
	DisplayName string `validate:"required" json:"display_name"`
	IsActive    bool   `validate:"required" json:"is_active"`
	GrpCode     string `validate:"required,min=1" json:"grp_code"`
}

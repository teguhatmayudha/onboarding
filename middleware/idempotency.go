package middleware

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"time"

	"bitbucket.org/finaccelteam/ms-common-go/errors"
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/ms-common-go/fhttp"
	"bitbucket.org/finaccelteam/ms-common-go/logger"
	"bitbucket.org/finaccelteam/ms-common-go/sql"
	"bitbucket.org/finaccelteam/ms-common-go/util/constants"
	domain "bitbucket.org/finaccelteam/onboarding/domain/idemPotency"
)

const (
	IdempotencyKeyLockTimeout = 300 // 5 minutes
)

type IdempotencyMiddleWare interface {
	Idempotency(ctx context.Context, req *http.Request, resp *fhttp.Response, fn func(ctx context.Context, resp *fhttp.Response) (*fhttp.Response, error)) (*fhttp.Response, error)
}

type idempotencyMiddleWare struct {
	tx   sql.Transactional
	repo domain.IdempotencyRepository
}

//Idempotency - Middleware to add Idempotency-Key to context
func (i *idempotencyMiddleWare) Idempotency(ctx context.Context, req *http.Request, resp *fhttp.Response, fn func(ctx context.Context, resp *fhttp.Response) (*fhttp.Response, error)) (*fhttp.Response, error) {
	idempotencyKeyID := req.Header.Get(constants.IdempotencyKey.String())
	if idempotencyKeyID == "" {
		errMsg := "Please provide Idempotency-Key in request header"
		logger.Error(req.Context(), errMsg)
		return nil, fhttp.NewResponseError("perror.IdempotencyKeyMissing", errMsg, http.StatusBadRequest)
	}
	requestBody := requestBodyToString(ctx, req.Body)
	record, err := i.IsRequestProcessed(ctx, req.Method, req.URL.Path, requestBody, idempotencyKeyID)
	if err != nil {
		errMsg := fmt.Sprintf("Failed to check idempotency for key %s. Error: %v", idempotencyKeyID, err.Error())
		if err != nil {
			return nil, fhttp.NewResponseError("perror.InternalError", errMsg, http.StatusBadRequest)
		}
	}

	/*
		if responseCode is non zero then responseBody.String has possible values
			1. ""
			2. non-empty data to return

		response data or response status might be incorrect(if code has bug
		and saved after service layer execution) but prevent to run service layer again for
		same idempotency key.
	*/

	if record.ResponseCode.Valid {
		logger.Info(ctx, "Returning response from idempotency check. Data %v.", idempotencyKeyID)
		resp.StatusCode = int(record.ResponseCode.Int64)
		err = json.Unmarshal([]byte(record.ResponseBody.String), &resp.Data)
		if err != nil {
			return nil, err
		}

		id, err := strconv.Atoi(record.ResponseBody.String)
		if err != nil {
			logger.Info(ctx, "convert string to integer")
		}

		return &fhttp.Response{
			Data:       id,
			StatusCode: int(record.ResponseCode.Int64),
		}, nil
	}
	//Add Idempotency-Key to context
	ctx = context.WithValue(req.Context(), constants.IdempotencyKey, idempotencyKeyID)
	//Add IncomingRequestID
	ctx = context.WithValue(ctx, "Incoming-Request-ID", idempotencyKeyID)
	ctx = logger.AddValueToContext(ctx, string(constants.IdempotencyKey), idempotencyKeyID)
	logger.Info(ctx, "Idempotency-Key: %v", idempotencyKeyID)

	if err := i.tx.WithTransaction(ctx, func(ctx context.Context) error {
		_, err := fn(ctx, resp)
		if err != nil {
			return err
		}

		respData, err := json.Marshal(resp.Data)
		if err != nil {
			return err
		}

		record.SetResponse(resp.StatusCode, string(respData))

		err = i.repo.Update(ctx, record)
		if err != nil {
			logger.Error(ctx, "%v", err.Error())
			return err
		}
		return nil
	}); err != nil {
		return nil, fhttp.NewResponseErrorWithCause("perror.InternalError", err, "Failed in idempotency", http.StatusInternalServerError)
	}

	return &fhttp.Response{
		Data:       resp.Data,
		StatusCode: resp.StatusCode,
	}, nil
}

func (i *idempotencyMiddleWare) IsRequestProcessed(ctx context.Context,
	requestMethod string,
	requestPath string,
	requetsParam string,
	idempotencyKeyID string) (*domain.Idempotency, error) {

	//check idempotency
	idempotencyKey, err := i.repo.FindByKeyAndRequestMethodAndRequestPath(ctx,
		idempotencyKeyID, requestMethod, requestPath)
	if err != nil {
		logger.Error(ctx, "Failed to get idempotency key %s. Error: %v",
			idempotencyKeyID, err)
		return nil, err
	}

	if idempotencyKey != nil {
		if idempotencyKey.ResponseBody.Valid {
			return idempotencyKey, nil
		}
		if idempotencyKey.LockedAt != nil && time.Since(*idempotencyKey.LockedAt).Seconds() < IdempotencyKeyLockTimeout {
			errMsg := fmt.Sprintf("Request is already in progress for idempotency key %s.", idempotencyKeyID)
			logger.Error(ctx, errMsg)
			return nil, errors.New(errMsg)
		}

		*idempotencyKey.LockedAt = time.Now().UTC()
		err = i.repo.Update(ctx, idempotencyKey)
		if err != nil {
			errMsg := fmt.Sprintf("Failed to update idempotencyKey for idempotencyKey id %d. Error: %v", idempotencyKey.ID, err.Error())
			logger.Error(ctx, errMsg)
			return nil, err
		}
	} else {
		idempotencyKey = &domain.Idempotency{}
		idempotencyKey.SetRequest(idempotencyKeyID, requestPath, requestMethod, requetsParam)
		id, err := i.repo.Save(ctx, idempotencyKey)
		if err != nil {
			errMsg := fmt.Sprintf("Failed to save idempotencyKey for request path: %s, method %s. Error: %v", requestPath, requestMethod, err.Error())
			logger.Error(ctx, "%v", errMsg)
			return nil, err
		}
		idempotencyKey.ID = id
	}

	return idempotencyKey, nil
}

func requestBodyToString(ctx context.Context, requestBody io.ReadCloser) string {
	buf := new(bytes.Buffer)
	if _, err := buf.ReadFrom(requestBody); err != nil {
		logger.Error(ctx, ferror.StackTrace(err))
	}
	return buf.String()
}

func marshalResponse(response string) ([]byte, error) {
	res, err := json.Marshal(response)
	if err != nil {
		return nil, ferror.E(fmt.Sprintf("Error parsing response body: %v", err.Error()))
	}
	return res, nil
}

func NewIdempotencyMiddleWare(tx sql.Transactional, repo domain.IdempotencyRepository) (IdempotencyMiddleWare, error) {
	return &idempotencyMiddleWare{
		tx:   tx,
		repo: repo,
	}, nil
}

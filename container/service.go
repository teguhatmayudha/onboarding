package container

import (
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/onboarding/service/partner"
	"bitbucket.org/finaccelteam/onboarding/service/partnerChannel"
)

var (
	partnerService        partner.Service
	partnerChannelService partnerChannel.Service
)

func getPartnerService() (partner.Service, error) {
	if partnerService == nil {
		partnerRepo, err := getPartnerRepository()
		if err != nil {
			return nil, ferror.Wrap(err, "get partner repository")
		}
		partnerService = partner.NewService(partnerRepo)
	}
	return partnerService, nil
}

func getPartnerChannelService() (partnerChannel.Service, error) {
	if partnerChannelService == nil {
		partnerRepo, err := getPartnerRepository()
		if err != nil {
			return nil, ferror.Wrap(err, "get partner repository")
		}
		partnerChannelRepo, err := getPartnerChannelRepository()
		if err != nil {
			return nil, ferror.Wrap(err, "get partner channel repository")
		}
		partnerChannelService = partnerChannel.NewService(partnerChannelRepo, partnerRepo)
	}
	return partnerChannelService, nil
}

package container

import (
	commonController "bitbucket.org/finaccelteam/ms-common-go/controller"
	"bitbucket.org/finaccelteam/onboarding/controller"
	"github.com/gorilla/mux"
)

//GetHomeController - initializes home controller
func GetHomeController(router *mux.Router) commonController.Controller {
	return controller.NewHome(router)
}




package container

import (
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/onboarding/domain/idemPotency"
	"bitbucket.org/finaccelteam/onboarding/domain/incomingEvent"
	"bitbucket.org/finaccelteam/onboarding/domain/outgoingEvent"
	"bitbucket.org/finaccelteam/onboarding/domain/partner"
	"bitbucket.org/finaccelteam/onboarding/domain/partnerChannel"
	"bitbucket.org/finaccelteam/onboarding/repository"
)

var (
	partnerRepository        partner.Repository
	partnerChannelRepository partnerChannel.Repository
	incomingEventRepository  incomingEvent.IncomingEventRepository
	idemPotencyRepository    idemPotency.IdempotencyRepository
	outgoingEventRepository  outgoingEvent.Repository
)

func getPartnerRepository() (partner.Repository, error) {
	if partnerRepository == nil {
		db, err := getDBTX()
		if err != nil {
			return nil, ferror.Wrap(err, "DB error")
		}
		partnerRepository = repository.NewPartnerRepository(db)
	}
	return partnerRepository, nil
}

func getPartnerChannelRepository() (partnerChannel.Repository, error) {
	if partnerChannelRepository == nil {
		db, err := getDBTX()
		if err != nil {
			return nil, ferror.Wrap(err, "DB error")
		}
		partnerChannelRepository = repository.NewPartnerChannelRepository(db)
	}
	return partnerChannelRepository, nil
}

func getIncomingEventRepository() (incomingEvent.IncomingEventRepository, error) {
	if incomingEventRepository == nil {
		db, err := getDBTX()
		if err != nil {
			return nil, ferror.Wrap(err, "DB error")
		}
		incomingEventRepository = repository.NewIncomingEventRepository(db)
	}
	return incomingEventRepository, nil
}

func getIdemPotencyRepository() (idemPotency.IdempotencyRepository, error) {
	if idemPotencyRepository == nil {
		db, err := getDBTX()
		if err != nil {
			return nil, ferror.Wrap(err, "DB error")
		}
		idemPotencyRepository = repository.NewIdempotencyRepository(db)
	}
	return idemPotencyRepository, nil

}

func getOutgoingEventRepository() (outgoingEvent.Repository, error) {
	if outgoingEventRepository == nil {
		db, err := getDBTX()
		if err != nil {
			return nil, ferror.Wrap(err, "DB error")
		}
		outgoingEventRepository = repository.NewOutgoingEventRepository(db)
	}
	return outgoingEventRepository, nil
}

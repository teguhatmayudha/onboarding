package container

import (
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/ms-common-go/fhttp"
	"bitbucket.org/finaccelteam/ms-common-go/sql"
	"context"
	dsql "database/sql"
	"github.com/gorilla/mux"
)

var (
	tx      sql.Transactional
	dbtx    *sql.DBTX
	queries sql.Queries
	db      *dsql.DB
)

//InitServer - initializes server
func InitServer(router *mux.Router) (*fhttp.Server, error) {
	config := GetServerConfig()
	httpConfig := &fhttp.Config{
		Name:    config.Name,
		Port:    config.Port,
		Version: config.Version,
	}
	server, err := fhttp.NewHTTPServer(httpConfig, router, fhttp.DefaultHealthCheckHandler())
	if err != nil {
		return nil, ferror.Wrap(err, "new http server")
	}
	return server, nil
}

func getDBTX() (*sql.DBTX, error) {
	var err error
	if dbtx == nil {
		dbConfig := GetMySQLDBConfig()
		config := sql.Config{
			Host:               dbConfig.GetHost(),
			Port:               dbConfig.GetPort(),
			User:               dbConfig.GetUser(),
			Password:           dbConfig.GetPassword(),
			Driver:             dbConfig.GetDriver(),
			Database:           dbConfig.GetDatabase(),
			MaxIdleConnections: dbConfig.GetMaxIdleConnections(),
			MaxOpenConnections: dbConfig.GetMaxOpenConnections(),
			UseAPM:             true,
		}

		dbtx, err = sql.NewDB(context.Background(), &config)
		if err != nil {
			return nil, err
		}
		return dbtx, nil
	}

	return dbtx, nil
}

func getTransactional(ctx context.Context) (sql.Transactional, error) {
	var err error
	if tx == nil {
		dbtx, err = getDBTX()
		if err != nil {
			return nil, err
		}

		tx, err = sql.NewTransactional(ctx, dbtx)
		if err != nil {
			return nil, err
		}
		return tx, nil
	}

	return tx, nil
}

func getNewSqlQueries() (sql.Queries, error) {
	return getDBTX()
}

// CloseConnections - Closes all active db and redis connections when called
func CloseConnections() error {
	if db != nil {
		if err := db.Close(); err != nil {
			return err
		}
	}

	return nil
}

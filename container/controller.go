package container

import (
	ferror "bitbucket.org/finaccelteam/ms-common-go/ferror/v2"
	"bitbucket.org/finaccelteam/onboarding/controller"
	"bitbucket.org/finaccelteam/onboarding/middleware"
	"context"
	"github.com/gorilla/mux"
)

func getIdemPotencyController(ctx context.Context) (middleware.IdempotencyMiddleWare, error) {
	trx, err := getTransactional(ctx)
	if err != nil {
		return nil, ferror.Wrap(err, "get transaction")
	}

	repo, err := getIdemPotencyRepository()
	if err != nil {
		return nil, ferror.Wrap(err, "get idem potency repository")
	}

	idempotency, err := middleware.NewIdempotencyMiddleWare(trx, repo)
	if err != nil {
		return nil, ferror.Wrap(err, "new idem potency middleware")
	}

	return idempotency, nil
}

func GetPartnerController(ctx context.Context, router *mux.Router) error {
	trx, err := getTransactional(ctx)
	if err != nil {
		return ferror.Wrap(err, "get transactional")
	}
	service, err := getPartnerService()
	if err != nil {
		return ferror.Wrap(err, "get partner service")
	}
	idempotency, err := getIdemPotencyController(ctx)
	if err != nil {
		return ferror.Wrap(err, "get idem potency controller")
	}
	controller.NewPartnerController(router, service, trx, idempotency)
	return nil
}

func GetPartnerChannelController(ctx context.Context, router *mux.Router) error {
	trx, err := getTransactional(ctx)
	if err != nil {
		return ferror.Wrap(err, "get transactional")
	}
	service, err := getPartnerChannelService()
	if err != nil {
		return ferror.Wrap(err, "get partner channel service")
	}
	idempotency, err := getIdemPotencyController(ctx)
	if err != nil {
		return ferror.Wrap(err, "get idem potency controller")
	}
	controller.NewPartnerChannelController(router, service, trx, idempotency)
	return nil
}

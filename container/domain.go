package container

import (
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/onboarding/domain/partnerChannel"
)

var (
	partnerChannelDomainService partnerChannel.Service
)

func getPartnerChannelDomainService() (partnerChannel.Service, error) {
	if partnerChannelDomainService == nil {
		partnerRepo, err := getPartnerRepository()
		if err != nil {
			return nil, ferror.Wrap(err, "get partner repo")
		}
		partnerChannelRepo, err := getPartnerChannelRepository()
		if err != nil {
			return nil, ferror.Wrap(err, "get partner channel repo")
		}

		partnerChannelDomainService = partnerChannel.NewService(partnerChannelRepo, partnerRepo)
	}
	return partnerChannelDomainService, nil
}
package container

import (
	"bitbucket.org/finaccelteam/ms-common-go/broker/kafka"
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	handler2 "bitbucket.org/finaccelteam/onboarding/broker/handler"
	"bitbucket.org/finaccelteam/onboarding/broker/kafka/publisher"
	"bitbucket.org/finaccelteam/onboarding/broker/kafka/subscriber"
	"bitbucket.org/finaccelteam/onboarding/handler"
	"context"
)

var (
	partnerDefaultEventHandler        handler.EventHandler
	partnerChannelDefaultEventHandler handler.EventHandler
)

func getIdemPotencyHandler(ctx context.Context) (*handler2.IdempotencyMiddleWare, error) {
	trx, err := getTransactional(ctx)
	if err != nil {
		return nil, ferror.Wrap(err, "get transactional")
	}
	repo, err := getIncomingEventRepository()
	if err != nil {
		return nil, ferror.Wrap(err, "get incoming event repository")
	}
	idemPotency, err := handler2.NewIdempotencyMiddleWare(trx, repo)
	if err != nil {
		return nil, ferror.Wrap(err, "new idempotency middleware")
	}
	return idemPotency, nil
}

func GetPartnerDefaultEventHandler(ctx context.Context, isDlq bool, numberOfConsumers int) (handler.EventHandler, error) {
	if partnerDefaultEventHandler == nil {
		service, err := getPartnerService()
		if err != nil {
			return nil, ferror.Wrap(err, "get partner service")
		}

		kafkaSubscriber, err := subscriber.GetKafkaEventSubscriber(handler.GetPartnerDefaultKafkaConfig(), isDlq, numberOfConsumers)
		if err != nil {
			return nil, ferror.Wrap(err, "get kafka event subscriber")
		}

		idemPotency, err := getIdemPotencyHandler(ctx)
		if err != nil {
			return nil, ferror.Wrap(err, "get idem potency")
		}

		kafkaSubscriber.Use(idemPotency.Middleware())

		incomingRepo, err := getIncomingEventRepository()
		if err != nil {
			return nil, ferror.Wrap(err, "get incoming repo")
		}

		outgoingRepo, err := getOutgoingEventRepository()
		if err != nil {
			return nil, ferror.Wrap(err, "get outgoing repo")
		}

		pub, err := publisher.GetKafkaEventPublisher()
		if err != nil {
			return nil, ferror.Wrap(err, "get kafka event publisher")
		}

		kafkaPublisher := pub.(*kafka.Publisher)

		partnerDefaultEventHandler, err = handler.NewPartnerDefault(kafkaSubscriber, service, kafkaPublisher, incomingRepo, outgoingRepo)
		if err != nil {
			return nil, ferror.Wrap(err, "new partner default handler")
		}

		handler.RegisterEventHandler(partnerDefaultEventHandler)
	}
	return partnerDefaultEventHandler, nil
}

func GetPartnerChannelDefaultEventHandler(ctx context.Context, isDlq bool, numberOfConsumers int) (handler.EventHandler, error) {
	if partnerDefaultEventHandler == nil {
		service, err := getPartnerChannelService()
		if err != nil {
			return nil, ferror.Wrap(err, "get partner channel service")
		}

		kafkaSubscriber, err := subscriber.GetKafkaEventSubscriber(handler.GetPartnerChannelDefaultKafkaConfig(), isDlq, numberOfConsumers)
		if err != nil {
			return nil, ferror.Wrap(err, "get kafka event subscriber")
		}

		idemPotency, err := getIdemPotencyHandler(ctx)
		if err != nil {
			return nil, ferror.Wrap(err, "get idem potency")
		}

		kafkaSubscriber.Use(idemPotency.Middleware())

		incomingRepo, err := getIncomingEventRepository()
		if err != nil {
			return nil, ferror.Wrap(err, "get incoming repo")
		}

		outgoingRepo, err := getOutgoingEventRepository()
		if err != nil {
			return nil, ferror.Wrap(err, "get outgoing repo")
		}

		pub, err := publisher.GetKafkaEventPublisher()
		if err != nil {
			return nil, ferror.Wrap(err, "get kafka event publisher")
		}

		kafkaPublisher := pub.(*kafka.Publisher)

		partnerChannelDefaultEventHandler, err = handler.NewPartnerChannelDefault(kafkaSubscriber, service, kafkaPublisher, incomingRepo, outgoingRepo)
		if err != nil {
			return nil, ferror.Wrap(err, "new partner channel default handler")
		}

		handler.RegisterEventHandler(partnerChannelDefaultEventHandler)
	}
	return partnerChannelDefaultEventHandler, nil
}

package container

var serverCfg Config

//Config - Struct containing the config data
type Config struct {
	Name    string `mapstructure:"name"`
	Port    int    `mapstructure:"port"`
	Version string `mapstructure:"version"`
}

//LoadInto - returns the struct c to load the config data into
func (s *Config) LoadInto() interface{} {
	return &s
}

//GetName - returns the name of the server
func (s *Config) GetName() string {
	return s.Name
}

//GetVersion - returns the Version of the server
func (s *Config) GetVersion() string {
	return s.Version
}

//GetPort - returns the port the server should run on
func (s *Config) GetPort() int {
	return s.Port
}

//GetServerConfig - returns the ServerConfig
func GetServerConfig() *Config {
	return &serverCfg
}

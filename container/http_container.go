package container

import (
	"context"
	"github.com/gorilla/mux"
)

func CreateHTTPContainer(ctx context.Context, router *mux.Router) error {
	GetHomeController(router)

	if err := GetPartnerController(ctx, router); err != nil {
		return err
	}

	if err := GetPartnerChannelController(ctx, router); err != nil {
		return err
	}

	return nil
}

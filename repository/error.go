package repository

import "bitbucket.org/finaccelteam/ms-common-go/ferror"

const (
	// ErrNotFoundCode is the super-type for any error that is if record does not Found.
	//
	// This error type IS not eligible for retry.
	ErrNotFoundCode = ferror.Code("PAYMENT_SERVICE-1001")
)

func ErrNotFoundRepository(m string, args ...interface{}) error {
	return ferror.Newf(ErrNotFoundCode, m, args...)
}
package repository

import (
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/ms-common-go/logger"
	"bitbucket.org/finaccelteam/ms-common-go/sql"
	domain "bitbucket.org/finaccelteam/onboarding/domain/partner"
	"context"
	"fmt"
	"strings"
)

var (
	allPartnerColNames = []string{
		partnerColID,
		partnerColName,
		partnerColMerchantID,
		partnerColGatewayType,
		partnerColIsActive,
		partnerColEntityID,
		partnerColTimezone,
		partnerColVersion,
		partnerColCreatedAt,
		partnerColCreatedBy,
		partnerColUpdatedAt,
		partnerColUpdatedBy,
		partnerColDeletedAt,
		partnerColDeletedBy,
	}

	partnerColNamesForInsert = []string{
		partnerColName,
		partnerColMerchantID,
		partnerColGatewayType,
		partnerColIsActive,
		partnerColEntityID,
		partnerColTimezone,
		partnerColVersion,
		partnerColCreatedBy,
		partnerColUpdatedBy,
		partnerColDeletedBy,
	}

	partnerColNamesForUpdate = []string{
		partnerColName,
		partnerColMerchantID,
		partnerColGatewayType,
		partnerColIsActive,
		partnerColVersion,
		partnerColUpdatedBy,
	}

	partnerColNamesWithNullValue = []string{
		partnerColDeletedBy,
	}
)

const (
	partnerTableName      = `partner`
	partnerColID          = `id`
	partnerColName        = `name`
	partnerColMerchantID  = `merchant_id`
	partnerColGatewayType = `gateway_type`
	partnerColIsActive    = `is_active`
	partnerColEntityID    = `entity_id`
	partnerColTimezone    = `timezone`
	partnerColVersion     = `version`
	partnerColCreatedAt   = `created_at`
	partnerColCreatedBy   = `created_by`
	partnerColUpdatedAt   = `updated_at`
	partnerColUpdatedBy   = `updated_by`
	partnerColDeletedAt   = `deleted_at`
	partnerColDeletedBy   = `deleted_by`
)

// Partner - Implementation of the Partner repo interface in domain
type Partner struct {
	db sql.Queries
}

// NewPartnerRepository - Creates and returns an implementation of the Partner
func NewPartnerRepository(db sql.Queries) domain.Repository {
	return &Partner{
		db: db,
	}
}

// Save - Function that persists the given Partner in the partner table
func (p *Partner) Save(ctx context.Context, partner *domain.Partner) (int, error) {
	colNames := strings.Join(partnerColNamesForInsert, ", ")
	placeholder := sql.GetQSPlaceholderForColumns(len(partnerColNamesForInsert))
	query := fmt.Sprintf("INSERT INTO %v(%v) VALUES (%v)", partnerTableName, colNames, placeholder)

	stmt, err := p.db.PrepareContext(ctx, query)
	if err != nil {
		return 0, ferror.Wrap(err, "prepare query")
	}

	defer func() {
		if err := stmt.Close(); err != nil {
			logger.Error(ctx, "Error in closing statement %v", err.Error())
		}
	}()

	res, err := stmt.ExecContext(ctx,
		partner.Name,
		partner.MerchantID,
		partner.GatewayType,
		partner.IsActive,
		partner.EntityID,
		partner.Timezone,
		partner.Version,
		partner.CreatedBy,
		partner.UpdatedBy,
		partner.DeletedBy,
	)
	if err != nil {
		return 0, ferror.Wrap(err, "exec stmt")
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, ferror.Wrap(err, "last insertID")
	}
	return int(id), nil
}

// FindByID - Function that returns Partner from partner table for given ID
func (p *Partner) FindByID(ctx context.Context, id int) (*domain.Partner, error) {
	columnNames := sql.GetSelectQSPlaceholderForColumns(allPartnerColNames)
	query := fmt.Sprintf("SELECT %v FROM %v WHERE %v = ? AND %v IS NULL", columnNames, partnerTableName, partnerColID, partnerColDeletedAt)

	var partner *domain.Partner
	scan := func(row sql.Row) error {
		tPartner := new(domain.Partner)
		if err := row.Scan(
			&tPartner.ID,
			&tPartner.Name,
			&tPartner.MerchantID,
			&tPartner.GatewayType,
			&tPartner.IsActive,
			&tPartner.EntityID,
			&tPartner.Timezone,
			&tPartner.Version,
			&tPartner.CreatedAt,
			&tPartner.CreatedBy,
			&tPartner.UpdatedAt,
			&tPartner.UpdatedBy,
			&tPartner.DeletedAt,
			&tPartner.DeletedBy,
		); err != nil {
			return ferror.Wrap(err, "row scan")
		}
		partner = tPartner
		return nil
	}
	if err := sql.Fetch(ctx, p.db, scan, query, id); err != nil {
		return nil, ferror.Wrap(err, "sql fetch")
	}
	return partner, nil
}

// Get - Function that returns Partner from partner table for given ID
func (p *Partner) Get(ctx context.Context) ([]*domain.Partner, error) {
	columnNames := sql.GetSelectQSPlaceholderForColumns(allPartnerColNames)
	query := fmt.Sprintf("SELECT %v FROM %v WHERE %v IS NULL", columnNames, partnerTableName, partnerColDeletedAt)
	partners := make([]*domain.Partner, 0)
	scan := func(row sql.Row) error {
		tPartner := new(domain.Partner)
		if err := row.Scan(
			&tPartner.ID,
			&tPartner.Name,
			&tPartner.MerchantID,
			&tPartner.GatewayType,
			&tPartner.IsActive,
			&tPartner.EntityID,
			&tPartner.Timezone,
			&tPartner.Version,
			&tPartner.CreatedAt,
			&tPartner.CreatedBy,
			&tPartner.UpdatedAt,
			&tPartner.UpdatedBy,
			&tPartner.DeletedAt,
			&tPartner.DeletedBy,
		); err != nil {
			return ferror.Wrap(err, "row scan")
		}
		partners = append(partners, tPartner)
		return nil
	}
	if err := sql.Fetch(ctx, p.db, scan, query); err != nil {
		return nil, ferror.Wrap(err, "sql fetch")
	}
	return partners, nil
}

func (p *Partner) FindByMerchantIDAndGatewayType(ctx context.Context, merchantID, gatewayType string) ([]*domain.Partner, error) {
	columnNames := sql.GetSelectQSPlaceholderForColumns(allPartnerColNames)
	query := fmt.Sprintf("SELECT %v FROM %v WHERE %v = ? AND %v = ? AND %v IS NULL", columnNames, partnerTableName, partnerColMerchantID, partnerColGatewayType, partnerColDeletedAt)
	partners := make([]*domain.Partner, 0)
	scan := func(row sql.Row) error {
		tPartner := new(domain.Partner)
		if err := row.Scan(
			&tPartner.ID,
			&tPartner.Name,
			&tPartner.MerchantID,
			&tPartner.GatewayType,
			&tPartner.IsActive,
			&tPartner.EntityID,
			&tPartner.Timezone,
			&tPartner.Version,
			&tPartner.CreatedAt,
			&tPartner.CreatedBy,
			&tPartner.UpdatedAt,
			&tPartner.UpdatedBy,
			&tPartner.DeletedAt,
			&tPartner.DeletedBy,
		); err != nil {
			return ferror.Wrap(err, "row scan")
		}
		partners = append(partners, tPartner)
		return nil
	}
	if err := sql.Fetch(ctx, p.db, scan, query, merchantID, gatewayType); err != nil {
		return nil, ferror.Wrap(err, "sql fetch")
	}
	return partners, nil
}
func (p *Partner) FindByGatewayType(ctx context.Context, gatewayType string) ([]*domain.Partner, error) {
	columnNames := sql.GetSelectQSPlaceholderForColumns(allPartnerColNames)
	query := fmt.Sprintf("SELECT %v FROM %v WHERE %v = ? AND %v IS NULL", columnNames, partnerTableName, partnerColGatewayType, partnerColDeletedAt)
	partners := make([]*domain.Partner, 0)

	//var partner *domain.Partner
	scan := func(row sql.Row) error {
		tPartner := new(domain.Partner)
		if err := row.Scan(
			&tPartner.ID,
			&tPartner.Name,
			&tPartner.MerchantID,
			&tPartner.GatewayType,
			&tPartner.IsActive,
			&tPartner.EntityID,
			&tPartner.Timezone,
			&tPartner.Version,
			&tPartner.CreatedAt,
			&tPartner.CreatedBy,
			&tPartner.UpdatedAt,
			&tPartner.UpdatedBy,
			&tPartner.DeletedAt,
			&tPartner.DeletedBy,
		); err != nil {
			return ferror.Wrap(err, "row scan")
		}
		partners = append(partners, tPartner)
		return nil
	}
	if err := sql.Fetch(ctx, p.db, scan, query, gatewayType); err != nil {
		return nil, ferror.Wrap(err, "sql fetch")
	}
	return partners, nil
}

// FindByMerchantID - Function that returns Partner from partner table for given ID
func (p *Partner) FindByMerchantID(ctx context.Context, merchantID string) ([]*domain.Partner, error) {
	columnNames := sql.GetSelectQSPlaceholderForColumns(allPartnerColNames)
	query := fmt.Sprintf("SELECT %v FROM %v WHERE %v = ? AND %v IS NULL", columnNames, partnerTableName, partnerColMerchantID, partnerColDeletedAt)
	partners := make([]*domain.Partner, 0)

	//var partner *domain.Partner
	scan := func(row sql.Row) error {
		tPartner := new(domain.Partner)
		if err := row.Scan(
			&tPartner.ID,
			&tPartner.Name,
			&tPartner.MerchantID,
			&tPartner.GatewayType,
			&tPartner.IsActive,
			&tPartner.EntityID,
			&tPartner.Timezone,
			&tPartner.Version,
			&tPartner.CreatedAt,
			&tPartner.CreatedBy,
			&tPartner.UpdatedAt,
			&tPartner.UpdatedBy,
			&tPartner.DeletedAt,
			&tPartner.DeletedBy,
		); err != nil {
			return ferror.Wrap(err, "row scan")
		}
		partners = append(partners, tPartner)
		return nil
	}
	if err := sql.Fetch(ctx, p.db, scan, query, merchantID); err != nil {
		return nil, ferror.Wrap(err, "sql fetch")
	}
	return partners, nil
}

func (p *Partner) Update(ctx context.Context, partner *domain.Partner) error {
	placeholder := sql.GetUpdateQSPlaceholderForColumns(partnerColNamesForUpdate)
	query := fmt.Sprintf("UPDATE %v SET %v WHERE  %v = %v AND version = %d", partnerTableName, placeholder, partnerColID, partner.ID, partner.Version)
	stmt, err := p.db.PrepareContext(ctx, query)
	if err != nil {
		return ferror.Wrap(err, "prepare query")
	}

	defer func() {
		if err := stmt.Close(); err != nil {
			logger.Error(ctx, "Error in closing statement %v", err.Error())
		}
	}()

	res, err := stmt.ExecContext(
		ctx,
		partner.Name,
		partner.MerchantID,
		partner.GatewayType,
		partner.IsActive,
		partner.Version+1,
		partner.UpdatedBy,
	)
	if err != nil {
		return ferror.Wrap(err, "execute stmt")
	}

	numberOfRows, err := res.RowsAffected()
	if err != nil {
		return ferror.Wrap(err, "rows affected result")
	}
	if numberOfRows == 0 {
		return ferror.Wrap(err, "Number of rows affected is zero ")
	}

	return nil
}

package repository

import (
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/ms-common-go/logger"
	"bitbucket.org/finaccelteam/ms-common-go/sql"
	"bitbucket.org/finaccelteam/onboarding/domain/incomingEvent"
	"fmt"
	"strings"
)

import (
	"context"
)

var (
	allIncomingEventColNames = []string{
		incomingEventColID,
		incomingEventColEventID,
		incomingEventColEventType,
		incomingEventColVersion,
		incomingEventColCreatedAt,
		incomingEventColUpdatedAt,
		incomingEventColDeletedAt,
	}

	incomingEventColNamesForInsert = []string{
		incomingEventColEventID,
		incomingEventColEventType,
		incomingEventColVersion,
	}

	incomingEventColNamesForUpdate = []string{
		incomingEventColEventID,
		incomingEventColEventType,
		incomingEventColVersion,
	}

	incomingEventColNamesWithNullValue = []string{}
)

const (
	incomingEventTableName    = `incoming_event`
	incomingEventColID        = `id`
	incomingEventColEventID   = `event_id`
	incomingEventColEventType = `event_type`
	incomingEventColVersion   = `version`
	incomingEventColCreatedAt = `created_at`
	incomingEventColUpdatedAt = `updated_at`
	incomingEventColDeletedAt = `deleted_at`
)

// IncomingEvent - Implementation of the IncomingEvent repo interface in domain
type IncomingEvent struct {
	db sql.Queries
}

// NewIncomingEventRepository - Creates and returns an implementation of the IncomingEvent
func NewIncomingEventRepository(db sql.Queries) incomingEvent.IncomingEventRepository {
	return &IncomingEvent{
		db: db,
	}
}

// Save - Function that persists the given IncomingEvent in the incoming_event table
func (ie *IncomingEvent) Save(ctx context.Context, incomingEvent *incomingEvent.IncomingEvent) (int, error) {
	colNames := strings.Join(incomingEventColNamesForInsert, ", ")
	placeholder := sql.GetQSPlaceholderForColumns(len(incomingEventColNamesForInsert))
	query := fmt.Sprintf("INSERT INTO %v(%v) VALUES (%v)", incomingEventTableName, colNames, placeholder)
	stmt, err := ie.db.PrepareContext(ctx, query)
	if err != nil {
		return 0, ferror.Wrap(err, "prepare context")
	}

	defer func() {
		if err := stmt.Close(); err != nil {
			logger.Error(ctx, "Error in closing statement %v", err.Error())
		}
	}()

	res, err := stmt.ExecContext(ctx,
		incomingEvent.EventID,
		incomingEvent.EventType,
		incomingEvent.Version,
	)
	if err != nil {
		return 0, ferror.Wrap(err, "execute stmt")
	}

	id, err := res.LastInsertId()
	if err != nil {
		return 0, ferror.Wrap(err, "last insertID")
	}

	return int(id), nil
}

// Update - Function that updates the given IncomingEvent in the incoming_event table
func (ie *IncomingEvent) Update(ctx context.Context, incomingEvent *incomingEvent.IncomingEvent) error {
	placeholder := sql.GetUpdateQSPlaceholderForColumns(incomingEventColNamesForUpdate)
	query := fmt.Sprintf("UPDATE %v SET %v WHERE id = %d", incomingEventTableName, placeholder, incomingEvent.ID)
	stmt, err := ie.db.PrepareContext(ctx, query)
	if err != nil {
		return ferror.Wrap(err, "prepare query")
	}

	defer func() {
		if err := stmt.Close(); err != nil {
			logger.Error(ctx, "Error in closing statement %v", err.Error())
		}
	}()

	_, err = stmt.ExecContext(ctx,
		incomingEvent.EventID,
		incomingEvent.EventType,
		incomingEvent.Version,
	)
	if err != nil {
		return ferror.Wrap(err, "exec stmt")
	}

	return nil
}

func scanIncomingEventRow(row sql.Row) (*incomingEvent.IncomingEvent, error) {
	incomingEvent := new(incomingEvent.IncomingEvent)

	if err := row.Scan(
		&incomingEvent.ID,
		&incomingEvent.EventID,
		&incomingEvent.EventType,
		&incomingEvent.Version,
		&incomingEvent.CreatedAt,
		&incomingEvent.UpdatedAt,
		&incomingEvent.DeletedAt,
	); err != nil {
		return nil, ferror.Wrap(err, "scan row")
	}

	return incomingEvent, nil
}

// FindByID - Function that returns IncomingEvent from incoming_event table for given ID
func (ie *IncomingEvent) FindByID(ctx context.Context, id int) (*incomingEvent.IncomingEvent, error) {
	columnNames := sql.GetSelectQSPlaceholderForColumns(allIncomingEventColNames)
	query := fmt.Sprintf("SELECT %v FROM %v WHERE %v = ? AND %v IS NULL", columnNames, incomingEventTableName, incomingEventColID, incomingEventColDeletedAt)

	var incomingEvent *incomingEvent.IncomingEvent
	scan := func(row sql.Row) error {
		tIncomingEvent, err := scanIncomingEventRow(row)
		if err != nil {
			return ferror.Wrap(err, "scan incoming event")
		}

		incomingEvent = tIncomingEvent
		return nil
	}

	if err := sql.Fetch(ctx, ie.db, scan, query, id); err != nil {
		return nil, ferror.Wrap(err, "sql fetch")
	}

	return incomingEvent, nil
}

// FindByEventID - Function that returns IncomingEvent from incoming_event table for given eventID
func (ie *IncomingEvent) FindByEventID(ctx context.Context, eventID string) (*incomingEvent.IncomingEvent, error) {
	columnNames := sql.GetSelectQSPlaceholderForColumns(allIncomingEventColNames)
	query := fmt.Sprintf("SELECT %v FROM %v WHERE %v = ? AND %v IS NULL", columnNames, incomingEventTableName, incomingEventColEventID, incomingEventColDeletedAt)

	var incomingEvent *incomingEvent.IncomingEvent
	scan := func(row sql.Row) error {
		tIncomingEvent, err := scanIncomingEventRow(row)
		if err != nil {
			return ferror.Wrap(err, "scan incoming event")
		}

		incomingEvent = tIncomingEvent
		return nil
	}
	if err := sql.Fetch(ctx, ie.db, scan, query, eventID); err != nil {
		return nil, ferror.Wrap(err, "sql fetch")
	}

	return incomingEvent, nil
}

// FindByEventIDAndEventType - Function that returns IncomingEvent from incoming_event table for given eventID and eventType
func (ie *IncomingEvent) FindByEventIDAndEventType(ctx context.Context, eventID string, eventType int) (*incomingEvent.IncomingEvent, error) {
	columnNames := sql.GetSelectQSPlaceholderForColumns(allIncomingEventColNames)
	query := fmt.Sprintf("SELECT %v FROM %v WHERE %v = ? AND %v = ? AND %v IS NULL", columnNames,
		incomingEventTableName,
		incomingEventColEventID,
		incomingEventColEventType,
		incomingEventColDeletedAt)

	var incomingEvent *incomingEvent.IncomingEvent
	scan := func(row sql.Row) error {
		tIncomingEvent, err := scanIncomingEventRow(row)
		if err != nil {
			return ferror.Wrap(err, "scan incoming event")
		}

		incomingEvent = tIncomingEvent
		return nil
	}
	if err := sql.Fetch(ctx, ie.db, scan, query, eventID, eventType); err != nil {
		return nil, ferror.Wrap(err, "sql fetch")
	}

	return incomingEvent, nil
}

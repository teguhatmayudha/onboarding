package response

type PartnerChannel struct {
	ID          int    `json:"id"`
	Code        string `json:"code"`
	PartnerID   int    `json:"partner_id"`
	Name        string `json:"name"`
	DisplayName string `json:"display_name"`
	IsActive    bool   `json:"is_active"`
}

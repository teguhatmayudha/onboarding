package response

type Partner struct {
	ID int `json:"id"`
	Name        string `json:"name"`
	MerchantID  string  `json:"merchant_id"`
	GatewayType int `json:"gateway_type"`
	IsActive    bool `json:"is_active"`
	EntityID    int `json:"entity_id"`
}

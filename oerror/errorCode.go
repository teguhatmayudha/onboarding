package oerror

import (
	ferror "bitbucket.org/finaccelteam/ms-common-go/ferror/v2"
)

const (
	PartnerChannelNotFound = ferror.Code("PARTNER_CHANNEL_NOT_FOUND")
	InternalError          = ferror.Code("INTERNAL_ERROR")
	BadMessage             = ferror.Code("BAD_MESSAGE")
	PartnerNotFound        = ferror.Code("PARTNER_NOT_FOUND")
	Unproccessable         = ferror.Code("UNPROCCESSABLE")
)

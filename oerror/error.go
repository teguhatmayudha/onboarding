package oerror

import (
	ferror "bitbucket.org/finaccelteam/ms-common-go/ferror/v2"
)

const ErrKindBusinessValidation = ferror.Kind("BUSINESS_VALIDATION")
const ErrKindInternalServerError = ferror.Kind("INTERNAL_SERVER_ERROR")
const ErrKindUnprocessable = ferror.Kind("UNPROCESSABLE")
const ErrKindNotFound = ferror.Kind("NOT_FOUND")

func NewBusinessValidationError(code ferror.Code, message string, args ...interface{}) error {
	return ferror.New(ErrKindBusinessValidation, code, message, args...)
}

func NewBusinessValidationErrorWithCause(code ferror.Code, err error, message string, args ...interface{}) error {
	return ferror.NewWithCause(ErrKindBusinessValidation, code, err, message, args...)
}

func NewInternalServerError(code ferror.Code, message string, args ...interface{}) error {
	return ferror.New(ErrKindInternalServerError, code, message, args...)
}

func NewInternalServerErrorWithCause(code ferror.Code, err error, message string, args ...interface{}) error {
	return ferror.NewWithCause(ErrKindInternalServerError, code, err, message, args...)
}
func NewUnprocessable(code ferror.Code, message string, args ...interface{}) error {
	return ferror.New(ErrKindUnprocessable, code, message, args...)
}

func NewUnprocessableWithCause(code ferror.Code, err error, message string, args ...interface{}) error {
	return ferror.NewWithCause(ErrKindUnprocessable, code, err, message, args...)
}
func NewNotFound(code ferror.Code, message string, args ...interface{}) error {
	return ferror.New(ErrKindNotFound, code, message, args...)
}

func NewNotFoundWithCause(code ferror.Code, err error, message string, args ...interface{}) error {
	return ferror.NewWithCause(ErrKindNotFound, code, err, message, args...)
}

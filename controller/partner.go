package controller

import (
	"context"
	"encoding/json"
	"net/http"
	"strconv"

	commonController "bitbucket.org/finaccelteam/ms-common-go/controller"
	"bitbucket.org/finaccelteam/ms-common-go/ferror"
	"bitbucket.org/finaccelteam/ms-common-go/fhttp"
	"bitbucket.org/finaccelteam/ms-common-go/sql"
	"bitbucket.org/finaccelteam/onboarding/middleware"
	"bitbucket.org/finaccelteam/onboarding/request"
	"bitbucket.org/finaccelteam/onboarding/response"
	"bitbucket.org/finaccelteam/onboarding/service/partner"
	"github.com/gorilla/mux"
)

type partnerController struct {
	service     partner.Service
	trx         sql.Transactional
	idempotency middleware.IdempotencyMiddleWare
}

func (p *partnerController) RegisterRoutes(router *mux.Router) {
	router.Handle("/partners", fhttp.AppHandler(p.getAll)).Methods(http.MethodGet)
	router.Handle("/partners/{id:[0-9]+}", fhttp.AppHandler(p.getPartner)).Methods(http.MethodGet)
	router.Handle("/partner", fhttp.AppHandler(p.createPartner)).Methods(http.MethodPost)
	router.Handle("/partner/{id:[0-9]+}", fhttp.AppHandler(p.updatePartner)).Methods(http.MethodPut)
}

func (p *partnerController) getAll(r *http.Request) (*fhttp.Response, error) {
	var (
		merchantID   *string
		gatewayType  *string
		err          error
		respPartners []response.Partner
	)

	query := r.URL.Query()
	for key := range query {
		if "merchantID" == key {
			merchantID = &query["merchantID"][0]
		} else {
			gatewayType = &query["gatewayType"][0]
		}
	}

	if merchantID != nil && gatewayType != nil {
		if err = p.trx.WithTransaction(r.Context(), func(ctx context.Context) error {
			respPartners, err = p.service.FindByMerchantIDAndGatewayType(ctx, *merchantID, *gatewayType)
			if err != nil {
				return buildErrorResponse(err, "")
			}
			return nil
		}); err != nil {
			return nil, buildErrorResponse(err, "")
		}
	} else if merchantID == nil && gatewayType != nil {
		if err = p.trx.WithTransaction(r.Context(), func(ctx context.Context) error {
			respPartners, err = p.service.FindByGatewayType(ctx, *gatewayType)
			if err != nil {
				return buildErrorResponse(err, "")
			}
			return nil
		}); err != nil {
			return nil, buildErrorResponse(err, "")
		}
	} else if merchantID != nil && gatewayType == nil {
		if err = p.trx.WithTransaction(r.Context(), func(ctx context.Context) error {
			respPartners, err = p.service.FindByMerchantID(ctx, *merchantID)
			if err != nil {
				return buildErrorResponse(err, "")
			}
			return nil
		}); err != nil {
			return nil, buildErrorResponse(err, "")
		}
	} else {
		if err = p.trx.WithTransaction(r.Context(), func(ctx context.Context) error {
			respPartners, err = p.service.GetPartners(ctx)
			if err != nil {
				return buildErrorResponse(err, "")
			}
			return nil
		}); err != nil {
			return nil, buildErrorResponse(err, "")
		}
	}

	return &fhttp.Response{
		Data:       respPartners,
		StatusCode: http.StatusOK,
	}, nil
}

func (p *partnerController) getPartner(r *http.Request) (*fhttp.Response, error) {
	var (
		respPartner *response.Partner
	)

	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		return nil, buildErrorResponse(err, "")
	}

	if err := p.trx.WithTransaction(r.Context(), func(ctx context.Context) error {
		respPartner, err = p.service.GetPartner(ctx, id)
		if err != nil {
			return ferror.Wrap(err, "get partner")
		}
		return nil
	}); err != nil {
		return nil, buildErrorResponse(err, "")
	}

	return &fhttp.Response{
		Data:       respPartner,
		StatusCode: http.StatusOK,
	}, nil
}

func (p *partnerController) createPartner(r *http.Request) (*fhttp.Response, error) {
	var (
		reqPartner request.Partner
	)

	if err := json.NewDecoder(r.Body).Decode(&reqPartner); err != nil {
		return nil, buildErrorResponse(err, "")
	}

	if _, err := validate(&reqPartner); err != nil {
		return nil, buildErrorResponse(err, "")
	}

	resp, err := p.idempotency.Idempotency(r.Context(), r, &fhttp.Response{}, func(ctx context.Context, resp *fhttp.Response) (*fhttp.Response, error) {
		id, err := p.service.CreatePartner(ctx, reqPartner)
		if err != nil {
			return nil, buildErrorResponse(err, "")
		}
		resp.Data = id
		resp.StatusCode = http.StatusCreated
		return nil, nil
	})
	if err != nil {
		return nil, buildErrorResponse(err, "")
	}

	return resp, nil
}

func (p *partnerController) updatePartner(r *http.Request) (*fhttp.Response, error) {
	var (
		reqPartner request.Partner
	)

	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		return nil, buildErrorResponse(err, "")
	}

	if err = json.NewDecoder(r.Body).Decode(&reqPartner); err != nil {
		return nil, buildErrorResponse(err, "")
	}

	if _, err = validate(&reqPartner); err != nil {
		return nil, buildErrorResponse(err, "")
	}

	resp, err := p.idempotency.Idempotency(r.Context(), r, &fhttp.Response{}, func(ctx context.Context, resp *fhttp.Response) (*fhttp.Response, error) {
		if err = p.service.UpdatePartner(ctx, reqPartner, id); err != nil {
			return nil, buildErrorResponse(err, "")
		}
		resp.Data = id
		resp.StatusCode = http.StatusCreated
		return nil, nil
	})
	if err != nil {
		return nil, buildErrorResponse(err, "")
	}

	return resp, nil
}

//NewPartnerController - Creates and returns a new NewPartnerController
func NewPartnerController(router *mux.Router, service partner.Service, trx sql.Transactional, idempotency middleware.IdempotencyMiddleWare) commonController.Controller {
	partnerController := &partnerController{
		service:     service,
		trx:         trx,
		idempotency: idempotency,
	}
	partnerController.RegisterRoutes(router)
	return partnerController
}

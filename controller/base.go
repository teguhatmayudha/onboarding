package controller

import (
	"net/http"

	ferror "bitbucket.org/finaccelteam/ms-common-go/ferror/v2"
	"bitbucket.org/finaccelteam/ms-common-go/fhttp"
	"bitbucket.org/finaccelteam/onboarding/oerror"
)

// TODO map error code to message
func buildErrorResponse(err error, locale string) error {
	if v, ok := err.(ferror.FError); ok {
		switch v.Kind() {
		case oerror.ErrKindInternalServerError:
			return fhttp.NewResponseErrorWithCause(oerror.InternalError, err, "", http.StatusInternalServerError)
		case oerror.ErrKindBusinessValidation:
			return fhttp.NewResponseErrorWithCause(oerror.BadMessage, err, "", http.StatusBadRequest)
		case oerror.ErrKindNotFound:
			return fhttp.NewResponseErrorWithCause(oerror.PartnerChannelNotFound, err, "", http.StatusNotFound)
		case oerror.ErrKindUnprocessable:
			return fhttp.NewResponseErrorWithCause(oerror.Unproccessable, err, "", http.StatusUnprocessableEntity)
		default:
			return fhttp.NewResponseErrorWithCause(oerror.InternalError, err, "", http.StatusInternalServerError)
		}
	} else {
		return fhttp.NewResponseErrorWithCause(oerror.InternalError, err, "", http.StatusInternalServerError)
	}
}

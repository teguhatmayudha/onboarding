package controller

import (
	"fmt"
	"github.com/go-playground/validator/v10"
)

// validate is used to validating the data from body to request.Partner
func validate(req interface{}) (string, error) {
	var errorMessages []string
	validate := validator.New()

	err := validate.Struct(req)
	if err != nil {
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("%s, condition %s %s", e.Field(), e.ActualTag(), e.Param())
			errorMessages = append(errorMessages, errorMessage)
		}
	}

	if errorMessages == nil {
		return "No error", nil
	}
	return errorMessages[0], err
}

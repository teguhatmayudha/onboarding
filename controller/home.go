package controller

import (
	commonController "bitbucket.org/finaccelteam/ms-common-go/controller"
	"net/http"

	"github.com/gorilla/mux"

	"bitbucket.org/finaccelteam/ms-common-go/fhttp"
)

type home struct{}

func (h home) RegisterRoutes(router *mux.Router) {
	router.Handle("/", fhttp.DefaultHealthCheckHandler()).Methods("GET")
	router.Handle("/healthcheck", fhttp.DefaultHealthCheckHandler()).Methods("GET")
}

func (h home) handler(r *http.Request) (*fhttp.Response, error) {
	return &fhttp.Response{
		StatusCode: http.StatusOK,
		Data:       "payment Service is Running.",
	}, nil
}

//NewHome - Creates and returns a new Home Controller
func NewHome(router *mux.Router) commonController.Controller {
	home := &home{}
	home.RegisterRoutes(router)
	return home
}

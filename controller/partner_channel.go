package controller

import (
	"context"
	"encoding/json"
	"net/http"
	"strconv"

	commonController "bitbucket.org/finaccelteam/ms-common-go/controller"
	"bitbucket.org/finaccelteam/ms-common-go/fhttp"
	"bitbucket.org/finaccelteam/ms-common-go/sql"
	"bitbucket.org/finaccelteam/onboarding/middleware"
	"bitbucket.org/finaccelteam/onboarding/request"
	"bitbucket.org/finaccelteam/onboarding/response"
	"bitbucket.org/finaccelteam/onboarding/service/partnerChannel"
	"github.com/gorilla/mux"
)

type partnerChannelController struct {
	service     partnerChannel.Service
	trx         sql.Transactional
	idempotency middleware.IdempotencyMiddleWare
}

func (p *partnerChannelController) RegisterRoutes(r *mux.Router) {
	r.Handle("/partnerChannel/{id:[0-9]+}", fhttp.AppHandler(p.getPartnerChannel)).Methods(http.MethodGet)
	r.Handle("/partnerChannels/{code}", fhttp.AppHandler(p.findByCode)).Methods(http.MethodGet)
	r.Handle("/partnerChannel", fhttp.AppHandler(p.createPartnerChannel)).Methods(http.MethodPost)
	r.Handle("/partnerChannel/{id:[0-9]+}", fhttp.AppHandler(p.updatePartnerChannel)).Methods(http.MethodPut)
}

func (p *partnerChannelController) getPartnerChannel(r *http.Request) (*fhttp.Response, error) {
	var respPartnerChannel *response.PartnerChannel

	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		return nil, buildErrorResponse(err, "")
	}

	if err = p.trx.WithTransaction(r.Context(), func(ctx context.Context) error {
		respPartnerChannel, err = p.service.GetPartnerChannel(r.Context(), id)
		if err != nil {
			return buildErrorResponse(err, "")
		}
		return nil
	}); err != nil {
		return nil, buildErrorResponse(err, "")
	}

	return &fhttp.Response{
		Data:       respPartnerChannel,
		StatusCode: http.StatusOK,
	}, nil
}

func (p *partnerChannelController) findByCode(r *http.Request) (*fhttp.Response, error) {
	var (
		resPartnerChannel *response.PartnerChannel
		err               error
	)

	code := mux.Vars(r)["code"]

	if err = p.trx.WithTransaction(r.Context(), func(ctx context.Context) error {
		resPartnerChannel, err = p.service.FindByCode(r.Context(), code)
		if err != nil {
			return buildErrorResponse(err, "")
		}
		return nil
	}); err != nil {
		return nil, buildErrorResponse(err, "")
	}

	return &fhttp.Response{
		Data:       resPartnerChannel,
		StatusCode: http.StatusOK,
	}, nil
}

func (p *partnerChannelController) createPartnerChannel(r *http.Request) (*fhttp.Response, error) {
	var (
		reqPartnerChannel request.PartnerChannel
	)

	if err := json.NewDecoder(r.Body).Decode(&reqPartnerChannel); err != nil {
		return nil, buildErrorResponse(err, "")
	}

	if _, err := validate(&reqPartnerChannel); err != nil {
		return nil, buildErrorResponse(err, "")
	}

	resp, err := p.idempotency.Idempotency(r.Context(), r, &fhttp.Response{}, func(ctx context.Context, resp *fhttp.Response) (*fhttp.Response, error) {
		id, err := p.service.CreatePartnerChannel(ctx, reqPartnerChannel)
		if err != nil {
			return nil, buildErrorResponse(err, "")
		}
		resp.Data = id
		resp.StatusCode = http.StatusCreated
		return nil, nil
	})
	if err != nil {
		return nil, buildErrorResponse(err, "")
	}

	return resp, nil
}

func (p *partnerChannelController) updatePartnerChannel(r *http.Request) (*fhttp.Response, error) {
	var (
		reqPartnerChannel request.PartnerChannel
	)

	id, err := strconv.Atoi(mux.Vars(r)["id"])
	if err != nil {
		return nil, buildErrorResponse(err, "")
	}

	if err := json.NewDecoder(r.Body).Decode(&reqPartnerChannel); err != nil {
		return nil, buildErrorResponse(err, "")
	}

	if _, err := validate(&reqPartnerChannel); err != nil {
		return nil, buildErrorResponse(err, "")
	}

	resp, err := p.idempotency.Idempotency(r.Context(), r, &fhttp.Response{}, func(ctx context.Context, resp *fhttp.Response) (*fhttp.Response, error) {
		if err = p.service.UpdatePartnerChannel(ctx, reqPartnerChannel, id); err != nil {
			return nil, buildErrorResponse(err, "")
		}
		resp.Data = id
		resp.StatusCode = http.StatusCreated
		return nil, nil
	})
	if err != nil {
		return nil, buildErrorResponse(err, "")
	}

	return resp, nil
}

//NewPartnerChannelController - Creates and returns a new NewPartnerChannelController
func NewPartnerChannelController(router *mux.Router, service partnerChannel.Service, trx sql.Transactional, idempotency middleware.IdempotencyMiddleWare) commonController.Controller {
	partnerChannelController := &partnerChannelController{
		service:     service,
		trx:         trx,
		idempotency: idempotency,
	}
	partnerChannelController.RegisterRoutes(router)
	return partnerChannelController
}
